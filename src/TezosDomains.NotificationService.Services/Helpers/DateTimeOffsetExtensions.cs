﻿namespace TezosDomains.NotificationService.Services.Helpers;

public static class DateTimeOffsetExtensions
{
    public static DateTimeOffset StartOfDay(this DateTimeOffset theDate)
    {
        return new DateTimeOffset(theDate.Date, theDate.Offset);
    }

    public static DateTimeOffset EndOfDay(this DateTimeOffset theDate)
    {
        return new DateTimeOffset(theDate.Date.AddDays(1).AddTicks(-1), theDate.Offset);
    }
}
