﻿using Microsoft.Extensions.Logging;
using MongoDB.Driver;

namespace TezosDomains.NotificationService.Services;

public class MongoDbInitializer
{
    private readonly ILogger _logger;
    private readonly MongoDbContext _mongoDbContext;

    public MongoDbInitializer(MongoDbContext mongoDbContext, ILogger<MongoDbInitializer> logger)
    {
        _mongoDbContext = mongoDbContext;
        _logger = logger;
    }

    public async Task Initialize(CancellationToken cancellationToken = default)
    {
        _logger.LogInformation("Initializing DB");
        await CreateIndexesAsync(
            _mongoDbContext.Subscriptions,
            cancellationToken,
            b => b.Combine(b.Ascending(s => s.Email), b.Ascending(s => s.Address)),
            b => b.Ascending(s => s.UnsubscribeKey)
        );
    }

    private static Task CreateIndexesAsync<T>(
        IMongoCollection<T> collection,
        CancellationToken cancellationToken,
        params Func<IndexKeysDefinitionBuilder<T>, IndexKeysDefinition<T>>[] indexes
    ) =>
        collection.Indexes.CreateManyAsync(
            indexes.Select(
                idx =>
                    new CreateIndexModel<T>(idx(Builders<T>.IndexKeys))
            ),
            cancellationToken
        );
}
