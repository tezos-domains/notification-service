﻿using SendGrid.Helpers.Mail;

namespace TezosDomains.NotificationService.Services.Configuration;

public interface IEmailConfiguration
{
    string BccLogAddress { get; }
    EmailAddress Sender { get; }
    IReadOnlyDictionary<string, string> Templates { get; }
    Uri AppUrl { get; }
}
