﻿namespace TezosDomains.NotificationService.Services.Configuration;

public interface IMongoDbConfiguration
{
    string ConnectionString { get; }
}
