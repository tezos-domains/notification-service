﻿namespace TezosDomains.NotificationService.Services.Configuration;

public interface ISubscriptionConfiguration
{
    TimeSpan? ConfirmKeyExpiration { get; }
}
