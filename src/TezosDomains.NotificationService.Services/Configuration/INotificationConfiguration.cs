﻿namespace TezosDomains.NotificationService.Services.Configuration;

public interface INotificationConfiguration
{
    Uri TezosDomainsApiUrl { get; }
    public string TezosDomainsApiClientId { get; }
    TimeSpan SkipBlocksOlderThan { get; }
    IReadOnlyDictionary<string, int> ExpiringSoonThresholds { get; }
    string SendAllExpirationNotificationsTo { get; }
}
