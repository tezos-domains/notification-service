﻿namespace TezosDomains.NotificationService.Services;

public class ServiceException : ApplicationException
{
    public ServiceExceptionReason Reason { get; }

    public ServiceException(ServiceExceptionReason reason, string message = null, Exception innerException = null) : base(message, innerException)
    {
        Reason = reason;
    }
}
