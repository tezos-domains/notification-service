﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using TezosDomains.NotificationService.Services.Configuration;
using TezosDomains.NotificationService.Services.Model;

namespace TezosDomains.NotificationService.Services;

public class MongoDbContext
{
    public IMongoClient Client { get; }
    public IMongoDatabase Database { get; }

    public IMongoCollection<Subscription> Subscriptions => Database.GetCollection<Subscription>("subscriptions");
    public IMongoCollection<State> State => Database.GetCollection<State>("state");

    static MongoDbContext()
    {
        BsonSerializer.RegisterSerializer(new GuidSerializer(BsonType.String));
    }

    public MongoDbContext(IMongoDbConfiguration configuration)
    {
        Client = new MongoClient(configuration.ConnectionString);
        Database = Client.GetDatabase(MongoUrl.Create(configuration.ConnectionString).DatabaseName);
    }
}
