﻿namespace TezosDomains.NotificationService.Services.Notifications;

public class Notification
{
    public string RecipientTezosAddress { get; set; }
    public NotificationType Type { get; set; }
    public object Data { get; set; }
    public int BlockLevel { get; set; }
    public DateTimeOffset BlockTimestamp { get; set; }
    public string SubType { get; set; }
}