﻿using TezosDomains.NotificationService.Services.GraphQL.Generated;

namespace TezosDomains.NotificationService.Services.Notifications;

public interface IEventToNotificationsConverter
{
    EventType EventType { get; }

    IReadOnlyList<Notification> Convert(IEvent @event);
}

public abstract class EventToNotificationsConverter<TEvent> : IEventToNotificationsConverter
{
    public abstract EventType EventType { get; }

    public IReadOnlyList<Notification> Convert(IEvent @event)
    {
        if (@event is TEvent e)
        {
            return CreateNotifications(e).ToList();
        }
        else
        {
            throw new Exception($"Expected event to be of type {typeof(TEvent).FullName}, but it was of type {@event.GetType().FullName}");
        }
    }

    protected abstract IEnumerable<Notification> CreateNotifications(TEvent @event);

    protected Notification CreateNotification(IEvent @event)
    {
        var notification = new Notification
        {
            BlockTimestamp = @event.Block.Timestamp,
            BlockLevel = @event.Block.Level
        };

        return notification;
    }
}
