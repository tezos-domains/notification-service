﻿namespace TezosDomains.NotificationService.Services.Notifications;

public enum NotificationType
{
    Outbid,
    AuctionWon,
    AuctionLost,
    OfferSold,
    BuyOfferPlaced,
    BuyOfferExecuted,
    ExpiringSoon
}
