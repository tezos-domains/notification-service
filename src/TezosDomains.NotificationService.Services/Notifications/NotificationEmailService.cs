﻿using Microsoft.Extensions.Logging;
using MongoDB.Driver;
using SendGrid.Helpers.Mail;
using TezosDomains.NotificationService.Services.Model;
using TezosDomains.NotificationService.Services.SendGrid;

namespace TezosDomains.NotificationService.Services.Notifications;

public class NotificationEmailService
{
    private readonly MongoDbContext _mongoDbContext;
    private readonly SendGridMessageFactory _sendGridMessageFactory;
    private readonly MailchainMessageFactory _mailchainMessageFactory;
    private readonly ILogger<NotificationEmailService> _log;

    public NotificationEmailService(MongoDbContext mongoDbContext, SendGridMessageFactory sendGridMessageFactory, ILogger<NotificationEmailService> log, MailchainMessageFactory mailchainMessageFactory)
    {
        _mongoDbContext = mongoDbContext;
        _sendGridMessageFactory = sendGridMessageFactory;
        _log = log;
        _mailchainMessageFactory = mailchainMessageFactory;
    }

    public async Task<List<SendGridMessage>> GenerateEmails(Notification notification, CancellationToken cancellationToken)
    {
        _log.LogDebug("Generating emails for notification of type {type} for address {address} with {data}", notification.Type, notification.RecipientTezosAddress, notification.Data);

        var subscriptions = await GetSubscriptionsForAddress(notification.RecipientTezosAddress, cancellationToken);

        var messages = subscriptions.Select(s => _sendGridMessageFactory.Create(s, GetTemplateName(notification), notification.Data)).ToList();

        _log.LogDebug("Generated {count} email messages", messages.Count);

        return messages;
    }

    public MailchainMessage GenerateMailchain(Notification notification)
    {
        _log.LogDebug("Generating mailchain messages for notification of type {type} for address {address} with {data}", notification.Type, notification.RecipientTezosAddress, notification.Data);

        return _mailchainMessageFactory.Create(notification.RecipientTezosAddress, GetTemplateName(notification), notification.Data);
    }

    private string GetTemplateName(Notification notification)
    {
        if (notification.SubType == null)
        {
            return notification.Type.ToString();
        }

        return $"{notification.Type}-{notification.SubType}";
    }

    private async Task<List<Subscription>> GetSubscriptionsForAddress(string address, CancellationToken cancellationToken)
    {
        var subscriptions = await _mongoDbContext.Subscriptions.FindAsync(
            Builders<Subscription>.Filter.And(
                Builders<Subscription>.Filter.Eq(s => s.Address, address),
                Builders<Subscription>.Filter.Eq(s => s.UnsubscribedAt, null),
                Builders<Subscription>.Filter.Ne(s => s.ConfirmedAt, null),
                Builders<Subscription>.Filter.Eq(s => s.ReplacedAt, null)
            ),
            cancellationToken: cancellationToken
        );

        return await subscriptions.ToListAsync(cancellationToken);
    }
}
