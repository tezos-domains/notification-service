﻿using TezosDomains.NotificationService.Services.GraphQL.Generated;

namespace TezosDomains.NotificationService.Services.Notifications.Converters;

public class BuyOfferPlacedEventConverter : EventToNotificationsConverter<BuyOfferPlacedEvent>
{
    public override EventType EventType => EventType.BuyOfferPlacedEvent;

    protected override IEnumerable<Notification> CreateNotifications(BuyOfferPlacedEvent @event)
    {
        var notification = CreateNotification(@event);

        notification.Type = NotificationType.BuyOfferPlaced;
        notification.RecipientTezosAddress = @event.DomainOwner;
        notification.Data = new
        {
            domainName = @event.DomainName,
            price = (@event.PriceWithoutFee / 1e6m).ToString("g6")
        };

        yield return notification;
    }
}
