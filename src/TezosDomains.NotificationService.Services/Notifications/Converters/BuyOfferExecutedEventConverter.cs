﻿using TezosDomains.NotificationService.Services.GraphQL.Generated;

namespace TezosDomains.NotificationService.Services.Notifications.Converters;

public class BuyOfferExecutedEventConverter : EventToNotificationsConverter<BuyOfferExecutedEvent>
{
    public override EventType EventType => EventType.BuyOfferExecutedEvent;

    protected override IEnumerable<Notification> CreateNotifications(BuyOfferExecutedEvent @event)
    {
        var notification = CreateNotification(@event);

        notification.Type = NotificationType.BuyOfferExecuted;
        notification.RecipientTezosAddress = @event.BuyerAddress;
        notification.Data = new
        {
            domainName = @event.DomainName,
        };

        yield return notification;
    }
}
