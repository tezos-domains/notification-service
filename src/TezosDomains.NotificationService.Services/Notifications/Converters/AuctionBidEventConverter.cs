﻿using TezosDomains.NotificationService.Services.GraphQL.Generated;

namespace TezosDomains.NotificationService.Services.Notifications.Converters;

public class AuctionBidEventConverter : EventToNotificationsConverter<AuctionBidEvent>
{
    public override EventType EventType => EventType.AuctionBidEvent;

    protected override IEnumerable<Notification> CreateNotifications(AuctionBidEvent @event)
    {
        if (@event.PreviousBidderAddress == null)
        {
            yield break;
        }

        var notification = CreateNotification(@event);

        notification.Type = NotificationType.Outbid;
        notification.RecipientTezosAddress = @event.PreviousBidderAddress;
        notification.Data = new
        {
            domainName = @event.DomainName,
            bidAmount = (@event.BidAmount / 1e6m).ToString("g6")
        };

        yield return notification;
    }
}
