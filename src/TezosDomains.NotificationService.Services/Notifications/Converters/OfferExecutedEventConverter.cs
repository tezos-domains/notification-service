﻿using TezosDomains.NotificationService.Services.GraphQL.Generated;

namespace TezosDomains.NotificationService.Services.Notifications.Converters;

public class OfferExecutedEventConverter : EventToNotificationsConverter<OfferExecutedEvent>
{
    public override EventType EventType => EventType.OfferExecutedEvent;

    protected override IEnumerable<Notification> CreateNotifications(OfferExecutedEvent @event)
    {
        var notification = CreateNotification(@event);

        notification.Type = NotificationType.OfferSold;
        notification.RecipientTezosAddress = @event.SellerAddress;
        notification.Data = new
        {
            domainName = @event.DomainName,
            price = (@event.PriceWithoutFee / 1e6m).ToString("g6")
        };

        yield return notification;
    }
}
