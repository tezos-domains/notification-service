﻿using TezosDomains.NotificationService.Services.GraphQL.Generated;

namespace TezosDomains.NotificationService.Services.Notifications.Converters;

public class AuctionEndEventConverter : EventToNotificationsConverter<AuctionEndEvent>
{
    public override EventType EventType => EventType.AuctionEndEvent;

    protected override IEnumerable<Notification> CreateNotifications(AuctionEndEvent @event)
    {
        var winNotification = CreateNotification(@event);

        winNotification.Type = NotificationType.AuctionWon;
        winNotification.RecipientTezosAddress = @event.SourceAddress;
        winNotification.Data = new
        {
            domainName = @event.DomainName
        };

        yield return winNotification;

        foreach (var participant in @event.Participants.Select(p => p.Address).Except(new[] { @event.SourceAddress }))
        {
            var lostNotification = CreateNotification(@event);

            lostNotification.Type = NotificationType.AuctionLost;
            lostNotification.RecipientTezosAddress = participant;
            lostNotification.Data = new
            {
                domainName = @event.DomainName
            };

            yield return lostNotification;
        }
    }
}
