﻿using MongoDB.Driver;
using TezosDomains.NotificationService.Services.Model;

namespace TezosDomains.NotificationService.Services.Notifications;

public class StateService
{
    private readonly Guid Id = new Guid("65a896c4-678e-4bfe-ba91-677630772d26");
    private readonly MongoDbContext _mongoDbContext;

    public StateService(MongoDbContext mongoDbContext)
    {
        _mongoDbContext = mongoDbContext;
    }

    public async Task<State> Get(CancellationToken cancellationToken)
    {
        var state = await GetState(cancellationToken);

        if (state != null)
        {
            return state;
        }

        await SetState(Builders<State>.Update.Combine(), cancellationToken);

        return await GetState(cancellationToken);
    }

    public async Task SetLastProcessedEvent(DateTimeOffset timestamp, CancellationToken cancellationToken)
    {
        await SetState(
            Builders<State>.Update.Set(
                s => s.LastProcessedEventTimestamp,
                timestamp
            ),
            cancellationToken
        );
    }

    public async Task SetLastProcessedExpiry(DateTimeOffset value, CancellationToken cancellationToken)
    {
        var state = await Get(cancellationToken);

        await SetState(
            Builders<State>.Update.Set(
                s => s.LastProcessedExpirationTimestamp,
                value
            ),
            cancellationToken
        );
    }

    private async Task SetState(UpdateDefinition<State> updates, CancellationToken cancellationToken)
    {
        await _mongoDbContext.State.UpdateOneAsync(
            Builders<State>.Filter.Eq(s => s.Id, Id),
            Builders<State>.Update.Combine(
                updates,
                Builders<State>.Update.SetOnInsert(s => s.Id, Id)
            ),
            new UpdateOptions { IsUpsert = true },
            cancellationToken
        );
    }

    private async Task<State> GetState(CancellationToken cancellationToken)
    {
        var states = await _mongoDbContext.State.FindAsync(
            Builders<State>.Filter.Eq(s => s.Id, Id),
            cancellationToken: cancellationToken
        );

        return await states.SingleOrDefaultAsync(cancellationToken);
    }
}
