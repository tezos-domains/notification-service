﻿using Microsoft.Extensions.DependencyInjection;
using TezosDomains.NotificationService.Services.Notifications.Converters;

namespace TezosDomains.NotificationService.Services.Notifications;

public static class NotificationsServiceCollectionExtensions
{
    public static IServiceCollection AddNotifications(this IServiceCollection services)
    {
        services
            .AddTransient<EventsProcessorService>()
            .AddTransient<ExpiringDomainsProcessorService>()
            .AddTransient<NotificationEmailService>()
            .AddTransient<StateService>()
            .AddTransient<IEventToNotificationsConverter, AuctionBidEventConverter>()
            .AddTransient<IEventToNotificationsConverter, AuctionEndEventConverter>()
            .AddTransient<IEventToNotificationsConverter, OfferExecutedEventConverter>()
            .AddTransient<IEventToNotificationsConverter, BuyOfferPlacedEventConverter>()
            .AddTransient<IEventToNotificationsConverter, BuyOfferExecutedEventConverter>();

        return services;
    }
}
