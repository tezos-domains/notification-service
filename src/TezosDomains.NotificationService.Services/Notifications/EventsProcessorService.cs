﻿using Microsoft.Extensions.Logging;
using TezosDomains.NotificationService.Services.Configuration;
using TezosDomains.NotificationService.Services.GraphQL.Generated;

namespace TezosDomains.NotificationService.Services.Notifications;

public class EventsProcessorService
{
    private readonly ITezosDomainsClient _tezosDomainsClient;
    private readonly StateService _stateService;
    private readonly INotificationConfiguration _notificationConfiguration;
    private readonly IClock _clock;
    private readonly IReadOnlyDictionary<EventType, IEventToNotificationsConverter> _eventConverters;
    private readonly ILogger<EventsProcessorService> _log;

    public EventsProcessorService(
        ITezosDomainsClient tezosDomainsClient,
        StateService stateService,
        INotificationConfiguration notificationConfiguration,
        IEnumerable<IEventToNotificationsConverter> eventConverters,
        IClock clock,
        ILogger<EventsProcessorService> log
    )
    {
        _tezosDomainsClient = tezosDomainsClient;
        _stateService = stateService;
        _notificationConfiguration = notificationConfiguration;
        _clock = clock;
        _eventConverters = eventConverters.ToDictionary(e => e.EventType);
        _log = log;
    }

    public async Task<List<Notification>> Process(CancellationToken cancellationToken)
    {
        var state = await _stateService.Get(cancellationToken);
        var from = state.LastProcessedEventTimestamp;

        if (IsOldBlock(from))
        {
            from = _clock.UtcNow.Subtract(_notificationConfiguration.SkipBlocksOlderThan);
        }

        _log.LogDebug("Getting events since {from}", from);

        var events = await GetEvents(from, cancellationToken);

        var notifications = events.SelectMany(CreateNotificationFromEvent).ToList();

        var lastEvent = events.LastOrDefault();
        if (lastEvent != null)
        {
            await _stateService.SetLastProcessedEvent(lastEvent.Block.Timestamp, cancellationToken);
        }

        _log.LogDebug("Created {count} notifications", notifications.Count);

        return notifications;
    }

    private async Task<List<IEvent>> GetEvents(DateTimeOffset timestamp, CancellationToken cancellationToken)
    {
        string after = null;
        var result = new List<IEvent>();

        while (true)
        {
            try
            {
                var events = await _tezosDomainsClient.EventsAsync(
                    first: 50,
                    after: after,
                    where: new EventsFilter
                    {
                        Type = new EventTypeFilter
                        {
                            In = new List<EventType>
                            {
                                EventType.AuctionBidEvent,
                                EventType.AuctionEndEvent,
                                EventType.OfferExecutedEvent,
                                EventType.BuyOfferPlacedEvent,
                                EventType.BuyOfferExecutedEvent,
                            }
                        },
                        Block = new AssociatedBlockFilter
                        {
                            Timestamp = new DateTimeFilter
                            {
                                GreaterThan = timestamp
                            }
                        }
                    },
                    order: new EventOrder
                    {
                        Field = EventOrderField.Timestamp,
                        Direction = OrderDirection.Asc
                    },
                    cancellationToken: cancellationToken
                );

                events.EnsureNoErrors();

                result.AddRange(events.Data!.Events.Edges.Select(e => e.Node));

                if (events.Data.Events.PageInfo.HasNextPage)
                {
                    after = events.Data.Events.PageInfo.EndCursor;
                }
                else
                {
                    break;
                }
            }
            catch (Exception e)
            {
                _log.LogError(e, "Error downloading blocks");
                break;
            }
        }

        _log.LogDebug("Downloaded {count} events", result.Count);

        return result;
    }

    private IReadOnlyList<Notification> CreateNotificationFromEvent(IEvent @event)
    {
        if (_eventConverters.TryGetValue(@event.Type, out var converter))
        {
            return converter.Convert(@event);
        }

        throw new Exception($"Converter for event type ${@event.Type} is not registered.");
    }

    private bool IsOldBlock(DateTimeOffset blockTimestamp)
    {
        return _clock.UtcNow.Subtract(blockTimestamp) > _notificationConfiguration.SkipBlocksOlderThan;
    }
}
