﻿using Microsoft.Extensions.Logging;
using TezosDomains.NotificationService.Services.Configuration;
using TezosDomains.NotificationService.Services.GraphQL.Generated;
using TezosDomains.NotificationService.Services.Helpers;

namespace TezosDomains.NotificationService.Services.Notifications;

public class ExpiringDomainsProcessorService
{
    private readonly ITezosDomainsClient _tezosDomainsClient;
    private readonly StateService _stateService;
    private readonly INotificationConfiguration _notificationConfig;
    private readonly IClock _clock;
    private readonly ILogger<ExpiringDomainsProcessorService> _log;

    public ExpiringDomainsProcessorService(
        ITezosDomainsClient tezosDomainsClient,
        StateService stateService,
        INotificationConfiguration notificationConfig,
        IClock clock,
        ILogger<ExpiringDomainsProcessorService> log
    )
    {
        _tezosDomainsClient = tezosDomainsClient;
        _stateService = stateService;
        _notificationConfig = notificationConfig;
        _clock = clock;
        _log = log;
    }

    public async Task<IReadOnlyCollection<Notification>> Process(CancellationToken cancellationToken)
    {
        var notifications = new List<Notification>();
        var now = _clock.UtcNow;
        var lastProcessedAt = await GetLastProcessedAt(cancellationToken);

        if (now < lastProcessedAt)
        {
            _log.LogDebug("Function already ran - cancelling execution");
            return Array.Empty<Notification>();
        }

        foreach (var threshold in _notificationConfig.ExpiringSoonThresholds)
        {
            var newNotifications = await CreateNotificationsFor(threshold.Key, threshold.Value, now, lastProcessedAt, cancellationToken);
            notifications.AddRange(newNotifications);
        }

        await SaveLastProcessed(now.EndOfDay(), cancellationToken);

        _log.LogDebug("Created {count} notifications", notifications.Count);

        return notifications.AsReadOnly();

    }



    private async Task<Notification[]> CreateNotificationsFor(string expirationType, int expirationThreshold, DateTimeOffset now, DateTimeOffset lastProcessedAt, CancellationToken cancellationToken)
    {
        var from = lastProcessedAt.Add(TimeSpan.FromDays(expirationThreshold));
        var to = now.EndOfDay().Add(TimeSpan.FromDays(expirationThreshold));

        _log.LogDebug("Getting domains from {from} to {to}", from, to);

        var domains = await GetExpiringDomains(from, to, cancellationToken);
        var notifications = GetNotifications(expirationType, domains, now);

        return notifications;
    }

    private Notification[] GetNotifications(string expirationType, IReadOnlyCollection<IDomain> domains, DateTimeOffset now)
    {
        // in case we don't process domains for a few days, we also need to group by expiration date so we show a consistent number of days before expiration in the email.
        var groupedDomains = domains.GroupBy(d => new { d.Owner, ExpiresAt = d.ExpiresAtUtc.Value.StartOfDay() });
        var notifications = groupedDomains.Select(group => CreateNotification(expirationType, group.Key.Owner, now, group.Key.ExpiresAt, group.ToList())).ToArray();

        _log.LogDebug("Created {count} notifications", notifications.Length);

        return notifications;
    }

    private async Task<DateTimeOffset> GetLastProcessedAt(CancellationToken cancellationToken)
    {
        var state = await _stateService.Get(cancellationToken);

        return state.LastProcessedExpirationTimestamp ?? DateTimeOffset.MinValue;
    }

    private async Task SaveLastProcessed(DateTimeOffset value, CancellationToken cancellationToken)
    {
        await _stateService.SetLastProcessedExpiry(value, cancellationToken);
    }

    private async Task<IReadOnlyCollection<IDomain>> GetExpiringDomains(DateTimeOffset from, DateTimeOffset to, CancellationToken cancellationToken)
    {
        string after = null;
        var result = new List<IDomain>();

        while (true)
        {
            try
            {
                var domains = await _tezosDomainsClient.DomainsAsync(
                    first: 50,
                    after: after,
                    where: new DomainsFilter
                    {
                        ExpiresAtUtc = new NullableDateTimeFilter
                        {
                            LessThanOrEqualTo = to,
                            GreaterThan = from
                        },
                        Level = new IntFilter
                        {
                            EqualTo = 2
                        }
                    },
                    order: new DomainOrder
                    {
                        Field = DomainOrderField.ExpiresAt,
                        Direction = OrderDirection.Asc
                    },
                    cancellationToken: cancellationToken
                );

                domains.EnsureNoErrors();

                result.AddRange(domains.Data!.Domains.Edges.Select(e => e.Node));

                if (domains.Data.Domains.PageInfo.HasNextPage)
                {
                    after = domains.Data.Domains.PageInfo.EndCursor;
                }
                else
                {
                    break;
                }
            }
            catch (Exception e)
            {
                _log.LogError(e, "Error downloading domains");
                throw;
            }
        }

        _log.LogDebug("Downloaded {count} expiring domains", result.Count);

        return result.AsReadOnly();
    }

    private Notification CreateNotification(string expirationType, string owner, DateTimeOffset now, DateTimeOffset expiryDay, IList<IDomain> domains)
    {
        var recipient = string.IsNullOrWhiteSpace(_notificationConfig.SendAllExpirationNotificationsTo) ? owner : _notificationConfig.SendAllExpirationNotificationsTo;

        return new Notification
        {
            Type = NotificationType.ExpiringSoon,
            SubType = expirationType,
            RecipientTezosAddress = recipient,
            Data = new
            {
                daysToExpiration = Math.Round((expiryDay - now.StartOfDay()).TotalDays),
                owner = recipient,
                domains = domains.Select(d => new
                {
                    expires = d.ExpiresAtUtc?.ToString("MMMM dd, yyyy"),
                    name = d.Name
                }).ToArray()
            }
        };
    }
}
