﻿using StrawberryShake.Serializers;

namespace TezosDomains.NotificationService.Services.GraphQL;

public class AddressValueSerializer : StringValueSerializer
{
    public override string Name => "Address";
}
