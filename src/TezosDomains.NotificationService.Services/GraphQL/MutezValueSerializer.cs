﻿using StrawberryShake;
using StrawberryShake.Serializers;

namespace TezosDomains.NotificationService.Services.GraphQL;

public class MutezValueSerializer : ValueSerializerBase<decimal, string>
{
    public override string Name => "Mutez";
    public override ValueKind Kind => ValueKind.String;
    
    public override object Serialize(object value)
    {
        throw new NotImplementedException();
    }

    public override object Deserialize(object serialized)
    {
        return serialized switch
        {
            null => null,
            string s => decimal.Parse(s),
            _ => throw new ArgumentException("The specified value is of an invalid type. " + $"{SerializationType.FullName} was expected.")
        };
    }
}
