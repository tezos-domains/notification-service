﻿using System;
using System.Collections;
using System.Collections.Generic;
using StrawberryShake;

namespace TezosDomains.NotificationService.Services.GraphQL.Generated
{
    [System.CodeDom.Compiler.GeneratedCode("StrawberryShake", "11.0.0")]
    public partial class NullableDateTimeFilterSerializer
        : IInputSerializer
    {
        private bool _needsInitialization = true;
        private IValueSerializer _dateTimeSerializer;
        private IValueSerializer _booleanSerializer;

        public string Name { get; } = "NullableDateTimeFilter";

        public ValueKind Kind { get; } = ValueKind.InputObject;

        public Type ClrType => typeof(NullableDateTimeFilter);

        public Type SerializationType => typeof(IReadOnlyDictionary<string, object>);

        public void Initialize(IValueSerializerCollection serializerResolver)
        {
            if (serializerResolver is null)
            {
                throw new ArgumentNullException(nameof(serializerResolver));
            }
            _dateTimeSerializer = serializerResolver.Get("DateTime");
            _booleanSerializer = serializerResolver.Get("Boolean");
            _needsInitialization = false;
        }

        public object Serialize(object value)
        {
            if (_needsInitialization)
            {
                throw new InvalidOperationException(
                    $"The serializer for type `{Name}` has not been initialized.");
            }

            if (value is null)
            {
                return null;
            }

            var input = (NullableDateTimeFilter)value;
            var map = new Dictionary<string, object>();

            if (input.EqualTo.HasValue)
            {
                map.Add("equalTo", SerializeNullableDateTime(input.EqualTo.Value));
            }

            if (input.GreaterThan.HasValue)
            {
                map.Add("greaterThan", SerializeNullableDateTime(input.GreaterThan.Value));
            }

            if (input.GreaterThanOrEqualTo.HasValue)
            {
                map.Add("greaterThanOrEqualTo", SerializeNullableDateTime(input.GreaterThanOrEqualTo.Value));
            }

            if (input.In.HasValue)
            {
                map.Add("in", SerializeNullableListOfDateTime(input.In.Value));
            }

            if (input.IsNull.HasValue)
            {
                map.Add("isNull", SerializeNullableBoolean(input.IsNull.Value));
            }

            if (input.LessThan.HasValue)
            {
                map.Add("lessThan", SerializeNullableDateTime(input.LessThan.Value));
            }

            if (input.LessThanOrEqualTo.HasValue)
            {
                map.Add("lessThanOrEqualTo", SerializeNullableDateTime(input.LessThanOrEqualTo.Value));
            }

            if (input.NotEqualTo.HasValue)
            {
                map.Add("notEqualTo", SerializeNullableDateTime(input.NotEqualTo.Value));
            }

            if (input.NotIn.HasValue)
            {
                map.Add("notIn", SerializeNullableListOfDateTime(input.NotIn.Value));
            }

            return map;
        }

        private object SerializeNullableDateTime(object value)
        {
            if (value is null)
            {
                return null;
            }


            return _dateTimeSerializer.Serialize(value);
        }

        private object SerializeNullableListOfDateTime(object value)
        {
            if (value is null)
            {
                return null;
            }


            IList source = (IList)value;
            object[] result = new object[source.Count];
            for(int i = 0; i < source.Count; i++)
            {
                result[i] = SerializeNullableDateTime(source[i]);
            }
            return result;
        }
        private object SerializeNullableBoolean(object value)
        {
            if (value is null)
            {
                return null;
            }


            return _booleanSerializer.Serialize(value);
        }

        public object Deserialize(object value)
        {
            throw new NotSupportedException(
                "Deserializing input values is not supported.");
        }
    }
}
