﻿using System;
using System.Collections;
using System.Collections.Generic;
using StrawberryShake;

namespace TezosDomains.NotificationService.Services.GraphQL.Generated
{
    [System.CodeDom.Compiler.GeneratedCode("StrawberryShake", "11.0.0")]
    public partial interface IAuctionBidEvent
    {
        string DomainName { get; }

        decimal BidAmount { get; }

        string PreviousBidderAddress { get; }
    }
}
