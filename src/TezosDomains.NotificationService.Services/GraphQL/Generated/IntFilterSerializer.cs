﻿using System;
using System.Collections;
using System.Collections.Generic;
using StrawberryShake;

namespace TezosDomains.NotificationService.Services.GraphQL.Generated
{
    [System.CodeDom.Compiler.GeneratedCode("StrawberryShake", "11.0.0")]
    public partial class IntFilterSerializer
        : IInputSerializer
    {
        private bool _needsInitialization = true;
        private IValueSerializer _intSerializer;

        public string Name { get; } = "IntFilter";

        public ValueKind Kind { get; } = ValueKind.InputObject;

        public Type ClrType => typeof(IntFilter);

        public Type SerializationType => typeof(IReadOnlyDictionary<string, object>);

        public void Initialize(IValueSerializerCollection serializerResolver)
        {
            if (serializerResolver is null)
            {
                throw new ArgumentNullException(nameof(serializerResolver));
            }
            _intSerializer = serializerResolver.Get("Int");
            _needsInitialization = false;
        }

        public object Serialize(object value)
        {
            if (_needsInitialization)
            {
                throw new InvalidOperationException(
                    $"The serializer for type `{Name}` has not been initialized.");
            }

            if (value is null)
            {
                return null;
            }

            var input = (IntFilter)value;
            var map = new Dictionary<string, object>();

            if (input.EqualTo.HasValue)
            {
                map.Add("equalTo", SerializeNullableInt(input.EqualTo.Value));
            }

            if (input.GreaterThan.HasValue)
            {
                map.Add("greaterThan", SerializeNullableInt(input.GreaterThan.Value));
            }

            if (input.GreaterThanOrEqualTo.HasValue)
            {
                map.Add("greaterThanOrEqualTo", SerializeNullableInt(input.GreaterThanOrEqualTo.Value));
            }

            if (input.In.HasValue)
            {
                map.Add("in", SerializeNullableListOfInt(input.In.Value));
            }

            if (input.LessThan.HasValue)
            {
                map.Add("lessThan", SerializeNullableInt(input.LessThan.Value));
            }

            if (input.LessThanOrEqualTo.HasValue)
            {
                map.Add("lessThanOrEqualTo", SerializeNullableInt(input.LessThanOrEqualTo.Value));
            }

            if (input.NotEqualTo.HasValue)
            {
                map.Add("notEqualTo", SerializeNullableInt(input.NotEqualTo.Value));
            }

            if (input.NotIn.HasValue)
            {
                map.Add("notIn", SerializeNullableListOfInt(input.NotIn.Value));
            }

            return map;
        }

        private object SerializeNullableInt(object value)
        {
            if (value is null)
            {
                return null;
            }


            return _intSerializer.Serialize(value);
        }

        private object SerializeNullableListOfInt(object value)
        {
            if (value is null)
            {
                return null;
            }


            IList source = (IList)value;
            object[] result = new object[source.Count];
            for(int i = 0; i < source.Count; i++)
            {
                result[i] = SerializeNullableInt(source[i]);
            }
            return result;
        }

        public object Deserialize(object value)
        {
            throw new NotSupportedException(
                "Deserializing input values is not supported.");
        }
    }
}
