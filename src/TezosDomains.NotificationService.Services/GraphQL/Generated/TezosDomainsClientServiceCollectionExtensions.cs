﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using StrawberryShake;
using StrawberryShake.Configuration;
using StrawberryShake.Http;
using StrawberryShake.Http.Pipelines;
using StrawberryShake.Http.Subscriptions;
using StrawberryShake.Serializers;
using StrawberryShake.Transport;

namespace TezosDomains.NotificationService.Services.GraphQL.Generated
{
    [System.CodeDom.Compiler.GeneratedCode("StrawberryShake", "11.0.0")]
    public static partial class TezosDomainsClientServiceCollectionExtensions
    {
        private const string _clientName = "TezosDomainsClient";

        public static IOperationClientBuilder AddTezosDomainsClient(
            this IServiceCollection serviceCollection)
        {
            if (serviceCollection is null)
            {
                throw new ArgumentNullException(nameof(serviceCollection));
            }

            serviceCollection.AddSingleton<ITezosDomainsClient, TezosDomainsClient>();

            serviceCollection.AddSingleton<IOperationExecutorFactory>(sp =>
                new HttpOperationExecutorFactory(
                    _clientName,
                    sp.GetRequiredService<IHttpClientFactory>().CreateClient,
                    sp.GetRequiredService<IClientOptions>().GetOperationPipeline<IHttpOperationContext>(_clientName),
                    sp.GetRequiredService<IClientOptions>().GetOperationFormatter(_clientName),
                    sp.GetRequiredService<IClientOptions>().GetResultParsers(_clientName)));

            IOperationClientBuilder builder = serviceCollection.AddOperationClientOptions(_clientName)
                .AddValueSerializer(() => new EventTypeValueSerializer())
                .AddValueSerializer(() => new OrderDirectionValueSerializer())
                .AddValueSerializer(() => new EventOrderFieldValueSerializer())
                .AddValueSerializer(() => new AddressPrefixValueSerializer())
                .AddValueSerializer(() => new RecordValidityValueSerializer())
                .AddValueSerializer(() => new DomainOrderFieldValueSerializer())
                .AddValueSerializer(() => new EventsFilterSerializer())
                .AddValueSerializer(() => new EventOrderSerializer())
                .AddValueSerializer(() => new AddressFilterSerializer())
                .AddValueSerializer(() => new AssociatedBlockFilterSerializer())
                .AddValueSerializer(() => new StringFilterSerializer())
                .AddValueSerializer(() => new MutezFilterSerializer())
                .AddValueSerializer(() => new EventTypeFilterSerializer())
                .AddValueSerializer(() => new StringEqualityFilterSerializer())
                .AddValueSerializer(() => new IntFilterSerializer())
                .AddValueSerializer(() => new DateTimeFilterSerializer())
                .AddValueSerializer(() => new StringLengthFilterSerializer())
                .AddValueSerializer(() => new DomainsFilterSerializer())
                .AddValueSerializer(() => new DomainOrderSerializer())
                .AddValueSerializer(() => new NullableAddressFilterSerializer())
                .AddValueSerializer(() => new StringArrayFilterSerializer())
                .AddValueSerializer(() => new NullableDateTimeFilterSerializer())
                .AddResultParser(serializers => new EventsResultParser(serializers))
                .AddResultParser(serializers => new DomainsResultParser(serializers))
                .AddOperationFormatter(serializers => new JsonOperationFormatter(serializers))
                .AddHttpOperationPipeline(b => b.UseHttpDefaultPipeline());

            serviceCollection.TryAddSingleton<IOperationExecutorPool, OperationExecutorPool>();
            return builder;
        }

    }
}
