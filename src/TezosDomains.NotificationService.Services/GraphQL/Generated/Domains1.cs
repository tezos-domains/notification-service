﻿using System;
using System.Collections;
using System.Collections.Generic;
using StrawberryShake;

namespace TezosDomains.NotificationService.Services.GraphQL.Generated
{
    [System.CodeDom.Compiler.GeneratedCode("StrawberryShake", "11.0.0")]
    public partial class Domains1
        : IDomains
    {
        public Domains1(
            global::TezosDomains.NotificationService.Services.GraphQL.Generated.IDomainConnection domains)
        {
            Domains = domains;
        }

        public global::TezosDomains.NotificationService.Services.GraphQL.Generated.IDomainConnection Domains { get; }
    }
}
