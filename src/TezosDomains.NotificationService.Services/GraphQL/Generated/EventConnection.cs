﻿using System;
using System.Collections;
using System.Collections.Generic;
using StrawberryShake;

namespace TezosDomains.NotificationService.Services.GraphQL.Generated
{
    [System.CodeDom.Compiler.GeneratedCode("StrawberryShake", "11.0.0")]
    public partial class EventConnection
        : IEventConnection
    {
        public EventConnection(
            global::System.Collections.Generic.IReadOnlyList<global::TezosDomains.NotificationService.Services.GraphQL.Generated.IEventEdge> edges, 
            global::TezosDomains.NotificationService.Services.GraphQL.Generated.IPageInfo pageInfo)
        {
            Edges = edges;
            PageInfo = pageInfo;
        }

        public global::System.Collections.Generic.IReadOnlyList<global::TezosDomains.NotificationService.Services.GraphQL.Generated.IEventEdge> Edges { get; }

        public global::TezosDomains.NotificationService.Services.GraphQL.Generated.IPageInfo PageInfo { get; }
    }
}
