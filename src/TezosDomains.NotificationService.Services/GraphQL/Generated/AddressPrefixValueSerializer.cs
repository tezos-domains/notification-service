﻿using System;
using System.Collections;
using System.Collections.Generic;
using StrawberryShake;

namespace TezosDomains.NotificationService.Services.GraphQL.Generated
{
    [System.CodeDom.Compiler.GeneratedCode("StrawberryShake", "11.0.0")]
    public partial class AddressPrefixValueSerializer
        : IValueSerializer
    {
        public string Name => "AddressPrefix";

        public ValueKind Kind => ValueKind.Enum;

        public Type ClrType => typeof(AddressPrefix);

        public Type SerializationType => typeof(string);

        public object Serialize(object value)
        {
            if (value is null)
            {
                return null;
            }

            var enumValue = (AddressPrefix)value;

            switch(enumValue)
            {
                case AddressPrefix.Kt1:
                    return "KT1";
                case AddressPrefix.Txr1:
                    return "TXR1";
                case AddressPrefix.Tz1:
                    return "TZ1";
                case AddressPrefix.Tz2:
                    return "TZ2";
                case AddressPrefix.Tz3:
                    return "TZ3";
                default:
                    throw new NotSupportedException();
            }
        }

        public object Deserialize(object serialized)
        {
            if (serialized is null)
            {
                return null;
            }

            var stringValue = (string)serialized;

            switch(stringValue)
            {
                case "KT1":
                    return AddressPrefix.Kt1;
                case "TXR1":
                    return AddressPrefix.Txr1;
                case "TZ1":
                    return AddressPrefix.Tz1;
                case "TZ2":
                    return AddressPrefix.Tz2;
                case "TZ3":
                    return AddressPrefix.Tz3;
                default:
                    throw new NotSupportedException();
            }
        }

    }
}
