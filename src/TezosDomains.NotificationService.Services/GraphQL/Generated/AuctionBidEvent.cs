﻿using System;
using System.Collections;
using System.Collections.Generic;
using StrawberryShake;

namespace TezosDomains.NotificationService.Services.GraphQL.Generated
{
    [System.CodeDom.Compiler.GeneratedCode("StrawberryShake", "11.0.0")]
    public partial class AuctionBidEvent
        : IEvent
        , IAuctionBidEvent1
    {
        public AuctionBidEvent(
            EventType type, 
            string sourceAddress, 
            global::TezosDomains.NotificationService.Services.GraphQL.Generated.IBlock block, 
            string domainName, 
            decimal bidAmount, 
            string previousBidderAddress)
        {
            Type = type;
            SourceAddress = sourceAddress;
            Block = block;
            DomainName = domainName;
            BidAmount = bidAmount;
            PreviousBidderAddress = previousBidderAddress;
        }

        public EventType Type { get; }

        public string SourceAddress { get; }

        public global::TezosDomains.NotificationService.Services.GraphQL.Generated.IBlock Block { get; }

        public string DomainName { get; }

        public decimal BidAmount { get; }

        public string PreviousBidderAddress { get; }
    }
}
