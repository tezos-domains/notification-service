﻿using System;
using System.Collections;
using System.Collections.Generic;
using StrawberryShake;

namespace TezosDomains.NotificationService.Services.GraphQL.Generated
{
    [System.CodeDom.Compiler.GeneratedCode("StrawberryShake", "11.0.0")]
    public partial class EventOrderFieldValueSerializer
        : IValueSerializer
    {
        public string Name => "EventOrderField";

        public ValueKind Kind => ValueKind.Enum;

        public Type ClrType => typeof(EventOrderField);

        public Type SerializationType => typeof(string);

        public object Serialize(object value)
        {
            if (value is null)
            {
                return null;
            }

            var enumValue = (EventOrderField)value;

            switch(enumValue)
            {
                case EventOrderField.Timestamp:
                    return "TIMESTAMP";
                default:
                    throw new NotSupportedException();
            }
        }

        public object Deserialize(object serialized)
        {
            if (serialized is null)
            {
                return null;
            }

            var stringValue = (string)serialized;

            switch(stringValue)
            {
                case "TIMESTAMP":
                    return EventOrderField.Timestamp;
                default:
                    throw new NotSupportedException();
            }
        }

    }
}
