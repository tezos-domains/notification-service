﻿using System;
using System.Collections;
using System.Collections.Generic;
using StrawberryShake;

namespace TezosDomains.NotificationService.Services.GraphQL.Generated
{
    [System.CodeDom.Compiler.GeneratedCode("StrawberryShake", "11.0.0")]
    public partial class AddressFilterSerializer
        : IInputSerializer
    {
        private bool _needsInitialization = true;
        private IValueSerializer _addressSerializer;
        private IValueSerializer _addressPrefixSerializer;

        public string Name { get; } = "AddressFilter";

        public ValueKind Kind { get; } = ValueKind.InputObject;

        public Type ClrType => typeof(AddressFilter);

        public Type SerializationType => typeof(IReadOnlyDictionary<string, object>);

        public void Initialize(IValueSerializerCollection serializerResolver)
        {
            if (serializerResolver is null)
            {
                throw new ArgumentNullException(nameof(serializerResolver));
            }
            _addressSerializer = serializerResolver.Get("Address");
            _addressPrefixSerializer = serializerResolver.Get("AddressPrefix");
            _needsInitialization = false;
        }

        public object Serialize(object value)
        {
            if (_needsInitialization)
            {
                throw new InvalidOperationException(
                    $"The serializer for type `{Name}` has not been initialized.");
            }

            if (value is null)
            {
                return null;
            }

            var input = (AddressFilter)value;
            var map = new Dictionary<string, object>();

            if (input.EqualTo.HasValue)
            {
                map.Add("equalTo", SerializeNullableAddress(input.EqualTo.Value));
            }

            if (input.In.HasValue)
            {
                map.Add("in", SerializeNullableListOfAddress(input.In.Value));
            }

            if (input.NotEqualTo.HasValue)
            {
                map.Add("notEqualTo", SerializeNullableAddress(input.NotEqualTo.Value));
            }

            if (input.NotIn.HasValue)
            {
                map.Add("notIn", SerializeNullableListOfAddress(input.NotIn.Value));
            }

            if (input.StartsWith.HasValue)
            {
                map.Add("startsWith", SerializeNullableAddressPrefix(input.StartsWith.Value));
            }

            return map;
        }

        private object SerializeNullableAddress(object value)
        {
            if (value is null)
            {
                return null;
            }


            return _addressSerializer.Serialize(value);
        }

        private object SerializeNullableListOfAddress(object value)
        {
            if (value is null)
            {
                return null;
            }


            IList source = (IList)value;
            object[] result = new object[source.Count];
            for(int i = 0; i < source.Count; i++)
            {
                result[i] = SerializeNullableAddress(source[i]);
            }
            return result;
        }
        private object SerializeNullableAddressPrefix(object value)
        {
            if (value is null)
            {
                return null;
            }


            return _addressPrefixSerializer.Serialize(value);
        }

        public object Deserialize(object value)
        {
            throw new NotSupportedException(
                "Deserializing input values is not supported.");
        }
    }
}
