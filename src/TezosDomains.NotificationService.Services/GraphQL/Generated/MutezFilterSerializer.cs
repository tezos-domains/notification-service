﻿using System;
using System.Collections;
using System.Collections.Generic;
using StrawberryShake;

namespace TezosDomains.NotificationService.Services.GraphQL.Generated
{
    [System.CodeDom.Compiler.GeneratedCode("StrawberryShake", "11.0.0")]
    public partial class MutezFilterSerializer
        : IInputSerializer
    {
        private bool _needsInitialization = true;
        private IValueSerializer _mutezSerializer;

        public string Name { get; } = "MutezFilter";

        public ValueKind Kind { get; } = ValueKind.InputObject;

        public Type ClrType => typeof(MutezFilter);

        public Type SerializationType => typeof(IReadOnlyDictionary<string, object>);

        public void Initialize(IValueSerializerCollection serializerResolver)
        {
            if (serializerResolver is null)
            {
                throw new ArgumentNullException(nameof(serializerResolver));
            }
            _mutezSerializer = serializerResolver.Get("Mutez");
            _needsInitialization = false;
        }

        public object Serialize(object value)
        {
            if (_needsInitialization)
            {
                throw new InvalidOperationException(
                    $"The serializer for type `{Name}` has not been initialized.");
            }

            if (value is null)
            {
                return null;
            }

            var input = (MutezFilter)value;
            var map = new Dictionary<string, object>();

            if (input.EqualTo.HasValue)
            {
                map.Add("equalTo", SerializeNullableMutez(input.EqualTo.Value));
            }

            if (input.GreaterThan.HasValue)
            {
                map.Add("greaterThan", SerializeNullableMutez(input.GreaterThan.Value));
            }

            if (input.GreaterThanOrEqualTo.HasValue)
            {
                map.Add("greaterThanOrEqualTo", SerializeNullableMutez(input.GreaterThanOrEqualTo.Value));
            }

            if (input.In.HasValue)
            {
                map.Add("in", SerializeNullableListOfMutez(input.In.Value));
            }

            if (input.LessThan.HasValue)
            {
                map.Add("lessThan", SerializeNullableMutez(input.LessThan.Value));
            }

            if (input.LessThanOrEqualTo.HasValue)
            {
                map.Add("lessThanOrEqualTo", SerializeNullableMutez(input.LessThanOrEqualTo.Value));
            }

            if (input.NotEqualTo.HasValue)
            {
                map.Add("notEqualTo", SerializeNullableMutez(input.NotEqualTo.Value));
            }

            if (input.NotIn.HasValue)
            {
                map.Add("notIn", SerializeNullableListOfMutez(input.NotIn.Value));
            }

            return map;
        }

        private object SerializeNullableMutez(object value)
        {
            if (value is null)
            {
                return null;
            }


            return _mutezSerializer.Serialize(value);
        }

        private object SerializeNullableListOfMutez(object value)
        {
            if (value is null)
            {
                return null;
            }


            IList source = (IList)value;
            object[] result = new object[source.Count];
            for(int i = 0; i < source.Count; i++)
            {
                result[i] = SerializeNullableMutez(source[i]);
            }
            return result;
        }

        public object Deserialize(object value)
        {
            throw new NotSupportedException(
                "Deserializing input values is not supported.");
        }
    }
}
