﻿using System;
using System.Collections;
using System.Collections.Generic;
using StrawberryShake;

namespace TezosDomains.NotificationService.Services.GraphQL.Generated
{
    [System.CodeDom.Compiler.GeneratedCode("StrawberryShake", "11.0.0")]
    public partial class BuyOfferExecutedEvent
        : IEvent
        , IBuyOfferExecutedEvent1
    {
        public BuyOfferExecutedEvent(
            EventType type, 
            string sourceAddress, 
            global::TezosDomains.NotificationService.Services.GraphQL.Generated.IBlock block, 
            string domainName, 
            string buyerAddress)
        {
            Type = type;
            SourceAddress = sourceAddress;
            Block = block;
            DomainName = domainName;
            BuyerAddress = buyerAddress;
        }

        public EventType Type { get; }

        public string SourceAddress { get; }

        public global::TezosDomains.NotificationService.Services.GraphQL.Generated.IBlock Block { get; }

        public string DomainName { get; }

        public string BuyerAddress { get; }
    }
}
