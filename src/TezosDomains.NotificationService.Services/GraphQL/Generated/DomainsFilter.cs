﻿using System;
using System.Collections;
using System.Collections.Generic;
using StrawberryShake;

namespace TezosDomains.NotificationService.Services.GraphQL.Generated
{
    [System.CodeDom.Compiler.GeneratedCode("StrawberryShake", "11.0.0")]
    public partial class DomainsFilter
    {
        public Optional<global::TezosDomains.NotificationService.Services.GraphQL.Generated.NullableAddressFilter> Address { get; set; }

        public Optional<global::TezosDomains.NotificationService.Services.GraphQL.Generated.StringArrayFilter> Ancestors { get; set; }

        public Optional<global::System.Collections.Generic.List<global::TezosDomains.NotificationService.Services.GraphQL.Generated.DomainsFilter>> And { get; set; }

        public Optional<global::TezosDomains.NotificationService.Services.GraphQL.Generated.NullableDateTimeFilter> ExpiresAtUtc { get; set; }

        public Optional<global::TezosDomains.NotificationService.Services.GraphQL.Generated.IntFilter> Level { get; set; }

        public Optional<global::TezosDomains.NotificationService.Services.GraphQL.Generated.StringFilter> Name { get; set; }

        public Optional<global::TezosDomains.NotificationService.Services.GraphQL.Generated.StringArrayFilter> Operators { get; set; }

        public Optional<global::System.Collections.Generic.List<global::TezosDomains.NotificationService.Services.GraphQL.Generated.DomainsFilter>> Or { get; set; }

        public Optional<global::TezosDomains.NotificationService.Services.GraphQL.Generated.AddressFilter> Owner { get; set; }

        public Optional<global::TezosDomains.NotificationService.Services.GraphQL.Generated.AddressFilter> ParentOwner { get; set; }

        public Optional<RecordValidity?> Validity { get; set; }
    }
}
