﻿using System;
using System.Collections;
using System.Collections.Generic;
using StrawberryShake;

namespace TezosDomains.NotificationService.Services.GraphQL.Generated
{
    [System.CodeDom.Compiler.GeneratedCode("StrawberryShake", "11.0.0")]
    public partial class DomainEdge
        : IDomainEdge
    {
        public DomainEdge(
            global::TezosDomains.NotificationService.Services.GraphQL.Generated.IDomain node)
        {
            Node = node;
        }

        public global::TezosDomains.NotificationService.Services.GraphQL.Generated.IDomain Node { get; }
    }
}
