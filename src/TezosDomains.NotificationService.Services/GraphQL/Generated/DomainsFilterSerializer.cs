﻿using System;
using System.Collections;
using System.Collections.Generic;
using StrawberryShake;

namespace TezosDomains.NotificationService.Services.GraphQL.Generated
{
    [System.CodeDom.Compiler.GeneratedCode("StrawberryShake", "11.0.0")]
    public partial class DomainsFilterSerializer
        : IInputSerializer
    {
        private bool _needsInitialization = true;
        private IValueSerializer _nullableAddressFilterSerializer;
        private IValueSerializer _stringArrayFilterSerializer;
        private IValueSerializer _domainsFilterSerializer;
        private IValueSerializer _nullableDateTimeFilterSerializer;
        private IValueSerializer _intFilterSerializer;
        private IValueSerializer _stringFilterSerializer;
        private IValueSerializer _addressFilterSerializer;
        private IValueSerializer _recordValiditySerializer;

        public string Name { get; } = "DomainsFilter";

        public ValueKind Kind { get; } = ValueKind.InputObject;

        public Type ClrType => typeof(DomainsFilter);

        public Type SerializationType => typeof(IReadOnlyDictionary<string, object>);

        public void Initialize(IValueSerializerCollection serializerResolver)
        {
            if (serializerResolver is null)
            {
                throw new ArgumentNullException(nameof(serializerResolver));
            }
            _nullableAddressFilterSerializer = serializerResolver.Get("NullableAddressFilter");
            _stringArrayFilterSerializer = serializerResolver.Get("StringArrayFilter");
            _domainsFilterSerializer = serializerResolver.Get("DomainsFilter");
            _nullableDateTimeFilterSerializer = serializerResolver.Get("NullableDateTimeFilter");
            _intFilterSerializer = serializerResolver.Get("IntFilter");
            _stringFilterSerializer = serializerResolver.Get("StringFilter");
            _addressFilterSerializer = serializerResolver.Get("AddressFilter");
            _recordValiditySerializer = serializerResolver.Get("RecordValidity");
            _needsInitialization = false;
        }

        public object Serialize(object value)
        {
            if (_needsInitialization)
            {
                throw new InvalidOperationException(
                    $"The serializer for type `{Name}` has not been initialized.");
            }

            if (value is null)
            {
                return null;
            }

            var input = (DomainsFilter)value;
            var map = new Dictionary<string, object>();

            if (input.Address.HasValue)
            {
                map.Add("address", SerializeNullableNullableAddressFilter(input.Address.Value));
            }

            if (input.Ancestors.HasValue)
            {
                map.Add("ancestors", SerializeNullableStringArrayFilter(input.Ancestors.Value));
            }

            if (input.And.HasValue)
            {
                map.Add("and", SerializeNullableListOfDomainsFilter(input.And.Value));
            }

            if (input.ExpiresAtUtc.HasValue)
            {
                map.Add("expiresAtUtc", SerializeNullableNullableDateTimeFilter(input.ExpiresAtUtc.Value));
            }

            if (input.Level.HasValue)
            {
                map.Add("level", SerializeNullableIntFilter(input.Level.Value));
            }

            if (input.Name.HasValue)
            {
                map.Add("name", SerializeNullableStringFilter(input.Name.Value));
            }

            if (input.Operators.HasValue)
            {
                map.Add("operators", SerializeNullableStringArrayFilter(input.Operators.Value));
            }

            if (input.Or.HasValue)
            {
                map.Add("or", SerializeNullableListOfDomainsFilter(input.Or.Value));
            }

            if (input.Owner.HasValue)
            {
                map.Add("owner", SerializeNullableAddressFilter(input.Owner.Value));
            }

            if (input.ParentOwner.HasValue)
            {
                map.Add("parentOwner", SerializeNullableAddressFilter(input.ParentOwner.Value));
            }

            if (input.Validity.HasValue)
            {
                map.Add("validity", SerializeNullableRecordValidity(input.Validity.Value));
            }

            return map;
        }

        private object SerializeNullableNullableAddressFilter(object value)
        {
            if (value is null)
            {
                return null;
            }


            return _nullableAddressFilterSerializer.Serialize(value);
        }
        private object SerializeNullableStringArrayFilter(object value)
        {
            if (value is null)
            {
                return null;
            }


            return _stringArrayFilterSerializer.Serialize(value);
        }
        private object SerializeNullableDomainsFilter(object value)
        {
            return _domainsFilterSerializer.Serialize(value);
        }

        private object SerializeNullableListOfDomainsFilter(object value)
        {
            if (value is null)
            {
                return null;
            }


            IList source = (IList)value;
            object[] result = new object[source.Count];
            for(int i = 0; i < source.Count; i++)
            {
                result[i] = SerializeNullableDomainsFilter(source[i]);
            }
            return result;
        }
        private object SerializeNullableNullableDateTimeFilter(object value)
        {
            if (value is null)
            {
                return null;
            }


            return _nullableDateTimeFilterSerializer.Serialize(value);
        }
        private object SerializeNullableIntFilter(object value)
        {
            if (value is null)
            {
                return null;
            }


            return _intFilterSerializer.Serialize(value);
        }
        private object SerializeNullableStringFilter(object value)
        {
            if (value is null)
            {
                return null;
            }


            return _stringFilterSerializer.Serialize(value);
        }
        private object SerializeNullableAddressFilter(object value)
        {
            if (value is null)
            {
                return null;
            }


            return _addressFilterSerializer.Serialize(value);
        }
        private object SerializeNullableRecordValidity(object value)
        {
            if (value is null)
            {
                return null;
            }


            return _recordValiditySerializer.Serialize(value);
        }

        public object Deserialize(object value)
        {
            throw new NotSupportedException(
                "Deserializing input values is not supported.");
        }
    }
}
