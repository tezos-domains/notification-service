﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using StrawberryShake;

namespace TezosDomains.NotificationService.Services.GraphQL.Generated
{
    [System.CodeDom.Compiler.GeneratedCode("StrawberryShake", "11.0.0")]
    public partial class TezosDomainsClient
        : ITezosDomainsClient
    {
        private const string _clientName = "TezosDomainsClient";

        private readonly global::StrawberryShake.IOperationExecutor _executor;

        public TezosDomainsClient(global::StrawberryShake.IOperationExecutorPool executorPool)
        {
            _executor = executorPool.CreateExecutor(_clientName);
        }

        public global::System.Threading.Tasks.Task<global::StrawberryShake.IOperationResult<global::TezosDomains.NotificationService.Services.GraphQL.Generated.IEvents>> EventsAsync(
            global::StrawberryShake.Optional<int?> first = default,
            global::StrawberryShake.Optional<string> after = default,
            global::StrawberryShake.Optional<global::TezosDomains.NotificationService.Services.GraphQL.Generated.EventsFilter> where = default,
            global::StrawberryShake.Optional<global::TezosDomains.NotificationService.Services.GraphQL.Generated.EventOrder> order = default,
            global::System.Threading.CancellationToken cancellationToken = default)
        {

            return _executor.ExecuteAsync(
                new EventsOperation
                {
                    First = first, 
                    After = after, 
                    Where = where, 
                    Order = order
                },
                cancellationToken);
        }

        public global::System.Threading.Tasks.Task<global::StrawberryShake.IOperationResult<global::TezosDomains.NotificationService.Services.GraphQL.Generated.IEvents>> EventsAsync(
            EventsOperation operation,
            global::System.Threading.CancellationToken cancellationToken = default)
        {
            if (operation is null)
            {
                throw new ArgumentNullException(nameof(operation));
            }

            return _executor.ExecuteAsync(operation, cancellationToken);
        }

        public global::System.Threading.Tasks.Task<global::StrawberryShake.IOperationResult<global::TezosDomains.NotificationService.Services.GraphQL.Generated.IDomains>> DomainsAsync(
            global::StrawberryShake.Optional<int?> first = default,
            global::StrawberryShake.Optional<string> after = default,
            global::StrawberryShake.Optional<global::TezosDomains.NotificationService.Services.GraphQL.Generated.DomainsFilter> where = default,
            global::StrawberryShake.Optional<global::TezosDomains.NotificationService.Services.GraphQL.Generated.DomainOrder> order = default,
            global::System.Threading.CancellationToken cancellationToken = default)
        {

            return _executor.ExecuteAsync(
                new DomainsOperation
                {
                    First = first, 
                    After = after, 
                    Where = where, 
                    Order = order
                },
                cancellationToken);
        }

        public global::System.Threading.Tasks.Task<global::StrawberryShake.IOperationResult<global::TezosDomains.NotificationService.Services.GraphQL.Generated.IDomains>> DomainsAsync(
            DomainsOperation operation,
            global::System.Threading.CancellationToken cancellationToken = default)
        {
            if (operation is null)
            {
                throw new ArgumentNullException(nameof(operation));
            }

            return _executor.ExecuteAsync(operation, cancellationToken);
        }
    }
}
