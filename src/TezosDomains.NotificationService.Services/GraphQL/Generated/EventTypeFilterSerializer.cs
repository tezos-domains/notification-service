﻿using System;
using System.Collections;
using System.Collections.Generic;
using StrawberryShake;

namespace TezosDomains.NotificationService.Services.GraphQL.Generated
{
    [System.CodeDom.Compiler.GeneratedCode("StrawberryShake", "11.0.0")]
    public partial class EventTypeFilterSerializer
        : IInputSerializer
    {
        private bool _needsInitialization = true;
        private IValueSerializer _eventTypeSerializer;

        public string Name { get; } = "EventTypeFilter";

        public ValueKind Kind { get; } = ValueKind.InputObject;

        public Type ClrType => typeof(EventTypeFilter);

        public Type SerializationType => typeof(IReadOnlyDictionary<string, object>);

        public void Initialize(IValueSerializerCollection serializerResolver)
        {
            if (serializerResolver is null)
            {
                throw new ArgumentNullException(nameof(serializerResolver));
            }
            _eventTypeSerializer = serializerResolver.Get("EventType");
            _needsInitialization = false;
        }

        public object Serialize(object value)
        {
            if (_needsInitialization)
            {
                throw new InvalidOperationException(
                    $"The serializer for type `{Name}` has not been initialized.");
            }

            if (value is null)
            {
                return null;
            }

            var input = (EventTypeFilter)value;
            var map = new Dictionary<string, object>();

            if (input.In.HasValue)
            {
                map.Add("in", SerializeNullableListOfEventType(input.In.Value));
            }

            if (input.NotIn.HasValue)
            {
                map.Add("notIn", SerializeNullableListOfEventType(input.NotIn.Value));
            }

            return map;
        }

        private object SerializeNullableEventType(object value)
        {
            return _eventTypeSerializer.Serialize(value);
        }

        private object SerializeNullableListOfEventType(object value)
        {
            if (value is null)
            {
                return null;
            }


            IList source = (IList)value;
            object[] result = new object[source.Count];
            for(int i = 0; i < source.Count; i++)
            {
                result[i] = SerializeNullableEventType(source[i]);
            }
            return result;
        }

        public object Deserialize(object value)
        {
            throw new NotSupportedException(
                "Deserializing input values is not supported.");
        }
    }
}
