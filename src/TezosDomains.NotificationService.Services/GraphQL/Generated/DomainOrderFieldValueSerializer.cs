﻿using System;
using System.Collections;
using System.Collections.Generic;
using StrawberryShake;

namespace TezosDomains.NotificationService.Services.GraphQL.Generated
{
    [System.CodeDom.Compiler.GeneratedCode("StrawberryShake", "11.0.0")]
    public partial class DomainOrderFieldValueSerializer
        : IValueSerializer
    {
        public string Name => "DomainOrderField";

        public ValueKind Kind => ValueKind.Enum;

        public Type ClrType => typeof(DomainOrderField);

        public Type SerializationType => typeof(string);

        public object Serialize(object value)
        {
            if (value is null)
            {
                return null;
            }

            var enumValue = (DomainOrderField)value;

            switch(enumValue)
            {
                case DomainOrderField.Address:
                    return "ADDRESS";
                case DomainOrderField.Domain:
                    return "DOMAIN";
                case DomainOrderField.ExpiresAt:
                    return "EXPIRES_AT";
                case DomainOrderField.Level:
                    return "LEVEL";
                case DomainOrderField.Name:
                    return "NAME";
                case DomainOrderField.Owner:
                    return "OWNER";
                default:
                    throw new NotSupportedException();
            }
        }

        public object Deserialize(object serialized)
        {
            if (serialized is null)
            {
                return null;
            }

            var stringValue = (string)serialized;

            switch(stringValue)
            {
                case "ADDRESS":
                    return DomainOrderField.Address;
                case "DOMAIN":
                    return DomainOrderField.Domain;
                case "EXPIRES_AT":
                    return DomainOrderField.ExpiresAt;
                case "LEVEL":
                    return DomainOrderField.Level;
                case "NAME":
                    return DomainOrderField.Name;
                case "OWNER":
                    return DomainOrderField.Owner;
                default:
                    throw new NotSupportedException();
            }
        }

    }
}
