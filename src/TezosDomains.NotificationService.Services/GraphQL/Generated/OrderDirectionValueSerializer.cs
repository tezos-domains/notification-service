﻿using System;
using System.Collections;
using System.Collections.Generic;
using StrawberryShake;

namespace TezosDomains.NotificationService.Services.GraphQL.Generated
{
    [System.CodeDom.Compiler.GeneratedCode("StrawberryShake", "11.0.0")]
    public partial class OrderDirectionValueSerializer
        : IValueSerializer
    {
        public string Name => "OrderDirection";

        public ValueKind Kind => ValueKind.Enum;

        public Type ClrType => typeof(OrderDirection);

        public Type SerializationType => typeof(string);

        public object Serialize(object value)
        {
            if (value is null)
            {
                return null;
            }

            var enumValue = (OrderDirection)value;

            switch(enumValue)
            {
                case OrderDirection.Asc:
                    return "ASC";
                case OrderDirection.Desc:
                    return "DESC";
                default:
                    throw new NotSupportedException();
            }
        }

        public object Deserialize(object serialized)
        {
            if (serialized is null)
            {
                return null;
            }

            var stringValue = (string)serialized;

            switch(stringValue)
            {
                case "ASC":
                    return OrderDirection.Asc;
                case "DESC":
                    return OrderDirection.Desc;
                default:
                    throw new NotSupportedException();
            }
        }

    }
}
