﻿using System;
using System.Collections;
using System.Collections.Generic;
using StrawberryShake;

namespace TezosDomains.NotificationService.Services.GraphQL.Generated
{
    [System.CodeDom.Compiler.GeneratedCode("StrawberryShake", "11.0.0")]
    public partial class StringLengthFilter
    {
        public Optional<int?> EqualTo { get; set; }

        public Optional<int?> GreaterThanOrEqualTo { get; set; }

        public Optional<int?> LessThanOrEqualTo { get; set; }
    }
}
