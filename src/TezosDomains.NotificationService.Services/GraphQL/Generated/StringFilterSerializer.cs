﻿using System;
using System.Collections;
using System.Collections.Generic;
using StrawberryShake;

namespace TezosDomains.NotificationService.Services.GraphQL.Generated
{
    [System.CodeDom.Compiler.GeneratedCode("StrawberryShake", "11.0.0")]
    public partial class StringFilterSerializer
        : IInputSerializer
    {
        private bool _needsInitialization = true;
        private IValueSerializer _stringSerializer;
        private IValueSerializer _stringLengthFilterSerializer;

        public string Name { get; } = "StringFilter";

        public ValueKind Kind { get; } = ValueKind.InputObject;

        public Type ClrType => typeof(StringFilter);

        public Type SerializationType => typeof(IReadOnlyDictionary<string, object>);

        public void Initialize(IValueSerializerCollection serializerResolver)
        {
            if (serializerResolver is null)
            {
                throw new ArgumentNullException(nameof(serializerResolver));
            }
            _stringSerializer = serializerResolver.Get("String");
            _stringLengthFilterSerializer = serializerResolver.Get("StringLengthFilter");
            _needsInitialization = false;
        }

        public object Serialize(object value)
        {
            if (_needsInitialization)
            {
                throw new InvalidOperationException(
                    $"The serializer for type `{Name}` has not been initialized.");
            }

            if (value is null)
            {
                return null;
            }

            var input = (StringFilter)value;
            var map = new Dictionary<string, object>();

            if (input.EndsWith.HasValue)
            {
                map.Add("endsWith", SerializeNullableString(input.EndsWith.Value));
            }

            if (input.EqualTo.HasValue)
            {
                map.Add("equalTo", SerializeNullableString(input.EqualTo.Value));
            }

            if (input.In.HasValue)
            {
                map.Add("in", SerializeNullableListOfString(input.In.Value));
            }

            if (input.Length.HasValue)
            {
                map.Add("length", SerializeNullableStringLengthFilter(input.Length.Value));
            }

            if (input.Like.HasValue)
            {
                map.Add("like", SerializeNullableString(input.Like.Value));
            }

            if (input.NotEqualTo.HasValue)
            {
                map.Add("notEqualTo", SerializeNullableString(input.NotEqualTo.Value));
            }

            if (input.NotIn.HasValue)
            {
                map.Add("notIn", SerializeNullableListOfString(input.NotIn.Value));
            }

            if (input.StartsWith.HasValue)
            {
                map.Add("startsWith", SerializeNullableString(input.StartsWith.Value));
            }

            return map;
        }

        private object SerializeNullableString(object value)
        {
            if (value is null)
            {
                return null;
            }


            return _stringSerializer.Serialize(value);
        }

        private object SerializeNullableListOfString(object value)
        {
            if (value is null)
            {
                return null;
            }


            IList source = (IList)value;
            object[] result = new object[source.Count];
            for(int i = 0; i < source.Count; i++)
            {
                result[i] = SerializeNullableString(source[i]);
            }
            return result;
        }
        private object SerializeNullableStringLengthFilter(object value)
        {
            if (value is null)
            {
                return null;
            }


            return _stringLengthFilterSerializer.Serialize(value);
        }

        public object Deserialize(object value)
        {
            throw new NotSupportedException(
                "Deserializing input values is not supported.");
        }
    }
}
