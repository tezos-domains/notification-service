﻿using System;
using System.Collections;
using System.Collections.Generic;
using StrawberryShake;

namespace TezosDomains.NotificationService.Services.GraphQL.Generated
{
    [System.CodeDom.Compiler.GeneratedCode("StrawberryShake", "11.0.0")]
    public partial class IntFilter
    {
        public Optional<int?> EqualTo { get; set; }

        public Optional<int?> GreaterThan { get; set; }

        public Optional<int?> GreaterThanOrEqualTo { get; set; }

        public Optional<IReadOnlyList<int>> In { get; set; }

        public Optional<int?> LessThan { get; set; }

        public Optional<int?> LessThanOrEqualTo { get; set; }

        public Optional<int?> NotEqualTo { get; set; }

        public Optional<IReadOnlyList<int>> NotIn { get; set; }
    }
}
