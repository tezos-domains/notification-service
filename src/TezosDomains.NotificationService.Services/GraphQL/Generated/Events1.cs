﻿using System;
using System.Collections;
using System.Collections.Generic;
using StrawberryShake;

namespace TezosDomains.NotificationService.Services.GraphQL.Generated
{
    [System.CodeDom.Compiler.GeneratedCode("StrawberryShake", "11.0.0")]
    public partial class Events1
        : IEvents
    {
        public Events1(
            global::TezosDomains.NotificationService.Services.GraphQL.Generated.IEventConnection events)
        {
            Events = events;
        }

        public global::TezosDomains.NotificationService.Services.GraphQL.Generated.IEventConnection Events { get; }
    }
}
