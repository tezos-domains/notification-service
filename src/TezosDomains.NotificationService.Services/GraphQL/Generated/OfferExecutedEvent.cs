﻿using System;
using System.Collections;
using System.Collections.Generic;
using StrawberryShake;

namespace TezosDomains.NotificationService.Services.GraphQL.Generated
{
    [System.CodeDom.Compiler.GeneratedCode("StrawberryShake", "11.0.0")]
    public partial class OfferExecutedEvent
        : IEvent
        , IOfferExecutedEvent1
    {
        public OfferExecutedEvent(
            EventType type, 
            string sourceAddress, 
            global::TezosDomains.NotificationService.Services.GraphQL.Generated.IBlock block, 
            string domainName, 
            decimal priceWithoutFee, 
            string sellerAddress)
        {
            Type = type;
            SourceAddress = sourceAddress;
            Block = block;
            DomainName = domainName;
            PriceWithoutFee = priceWithoutFee;
            SellerAddress = sellerAddress;
        }

        public EventType Type { get; }

        public string SourceAddress { get; }

        public global::TezosDomains.NotificationService.Services.GraphQL.Generated.IBlock Block { get; }

        public string DomainName { get; }

        public decimal PriceWithoutFee { get; }

        public string SellerAddress { get; }
    }
}
