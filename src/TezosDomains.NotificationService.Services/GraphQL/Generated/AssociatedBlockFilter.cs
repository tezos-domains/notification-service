﻿using System;
using System.Collections;
using System.Collections.Generic;
using StrawberryShake;

namespace TezosDomains.NotificationService.Services.GraphQL.Generated
{
    [System.CodeDom.Compiler.GeneratedCode("StrawberryShake", "11.0.0")]
    public partial class AssociatedBlockFilter
    {
        public Optional<global::TezosDomains.NotificationService.Services.GraphQL.Generated.StringEqualityFilter> Hash { get; set; }

        public Optional<global::TezosDomains.NotificationService.Services.GraphQL.Generated.IntFilter> Level { get; set; }

        public Optional<global::TezosDomains.NotificationService.Services.GraphQL.Generated.DateTimeFilter> Timestamp { get; set; }
    }
}
