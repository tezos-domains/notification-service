﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.Json;
using StrawberryShake;
using StrawberryShake.Configuration;
using StrawberryShake.Http;
using StrawberryShake.Http.Subscriptions;
using StrawberryShake.Transport;

namespace TezosDomains.NotificationService.Services.GraphQL.Generated
{
    [System.CodeDom.Compiler.GeneratedCode("StrawberryShake", "11.0.0")]
    public partial class EventsResultParser
        : JsonResultParserBase<IEvents>
    {
        private readonly IValueSerializer _stringSerializer;
        private readonly IValueSerializer _booleanSerializer;
        private readonly IValueSerializer _eventTypeSerializer;
        private readonly IValueSerializer _addressSerializer;
        private readonly IValueSerializer _mutezSerializer;
        private readonly IValueSerializer _intSerializer;
        private readonly IValueSerializer _dateTimeSerializer;

        public EventsResultParser(IValueSerializerCollection serializerResolver)
        {
            if (serializerResolver is null)
            {
                throw new ArgumentNullException(nameof(serializerResolver));
            }
            _stringSerializer = serializerResolver.Get("String");
            _booleanSerializer = serializerResolver.Get("Boolean");
            _eventTypeSerializer = serializerResolver.Get("EventType");
            _addressSerializer = serializerResolver.Get("Address");
            _mutezSerializer = serializerResolver.Get("Mutez");
            _intSerializer = serializerResolver.Get("Int");
            _dateTimeSerializer = serializerResolver.Get("DateTime");
        }

        protected override IEvents ParserData(JsonElement data)
        {
            return new Events1
            (
                ParseEventsEvents(data, "events")
            );

        }

        private global::TezosDomains.NotificationService.Services.GraphQL.Generated.IEventConnection ParseEventsEvents(
            JsonElement parent,
            string field)
        {
            JsonElement obj = parent.GetProperty(field);

            return new EventConnection
            (
                ParseEventsEventsEdges(obj, "edges"),
                ParseEventsEventsPageInfo(obj, "pageInfo")
            );
        }

        private global::System.Collections.Generic.IReadOnlyList<global::TezosDomains.NotificationService.Services.GraphQL.Generated.IEventEdge> ParseEventsEventsEdges(
            JsonElement parent,
            string field)
        {
            JsonElement obj = parent.GetProperty(field);

            int objLength = obj.GetArrayLength();
            var list = new global::TezosDomains.NotificationService.Services.GraphQL.Generated.IEventEdge[objLength];
            for (int objIndex = 0; objIndex < objLength; objIndex++)
            {
                JsonElement element = obj[objIndex];
                list[objIndex] = new EventEdge
                (
                    ParseEventsEventsEdgesNode(element, "node")
                );

            }

            return list;
        }

        private global::TezosDomains.NotificationService.Services.GraphQL.Generated.IPageInfo ParseEventsEventsPageInfo(
            JsonElement parent,
            string field)
        {
            JsonElement obj = parent.GetProperty(field);

            return new PageInfo
            (
                DeserializeNullableString(obj, "endCursor"),
                DeserializeBoolean(obj, "hasNextPage")
            );
        }

        private global::TezosDomains.NotificationService.Services.GraphQL.Generated.IEvent ParseEventsEventsEdgesNode(
            JsonElement parent,
            string field)
        {
            JsonElement obj = parent.GetProperty(field);

            string type = obj.GetProperty(TypeName).GetString();

            switch(type)
            {
                case "AuctionBidEvent":
                    return new AuctionBidEvent
                    (
                        DeserializeEventType(obj, "type"),
                        DeserializeAddress(obj, "sourceAddress"),
                        ParseEventsEventsEdgesNodeBlock(obj, "block"),
                        DeserializeString(obj, "domainName"),
                        DeserializeMutez(obj, "bidAmount"),
                        DeserializeNullableAddress(obj, "previousBidderAddress")
                    );

                case "AuctionEndEvent":
                    return new AuctionEndEvent
                    (
                        DeserializeEventType(obj, "type"),
                        DeserializeAddress(obj, "sourceAddress"),
                        ParseEventsEventsEdgesNodeBlock(obj, "block"),
                        DeserializeString(obj, "domainName"),
                        ParseEventsEventsEdgesNodeParticipants(obj, "participants")
                    );

                case "AuctionSettleEvent":
                    return new AuctionSettleEvent
                    (
                        DeserializeEventType(obj, "type"),
                        DeserializeAddress(obj, "sourceAddress"),
                        ParseEventsEventsEdgesNodeBlock(obj, "block")
                    );

                case "AuctionWithdrawEvent":
                    return new AuctionWithdrawEvent
                    (
                        DeserializeEventType(obj, "type"),
                        DeserializeAddress(obj, "sourceAddress"),
                        ParseEventsEventsEdgesNodeBlock(obj, "block")
                    );

                case "BuyOfferExecutedEvent":
                    return new BuyOfferExecutedEvent
                    (
                        DeserializeEventType(obj, "type"),
                        DeserializeAddress(obj, "sourceAddress"),
                        ParseEventsEventsEdgesNodeBlock(obj, "block"),
                        DeserializeString(obj, "domainName"),
                        DeserializeAddress(obj, "buyerAddress")
                    );

                case "BuyOfferPlacedEvent":
                    return new BuyOfferPlacedEvent
                    (
                        DeserializeEventType(obj, "type"),
                        DeserializeAddress(obj, "sourceAddress"),
                        ParseEventsEventsEdgesNodeBlock(obj, "block"),
                        DeserializeString(obj, "domainName"),
                        DeserializeString(obj, "domainOwner"),
                        DeserializeMutez(obj, "priceWithoutFee")
                    );

                case "BuyOfferRemovedEvent":
                    return new BuyOfferRemovedEvent
                    (
                        DeserializeEventType(obj, "type"),
                        DeserializeAddress(obj, "sourceAddress"),
                        ParseEventsEventsEdgesNodeBlock(obj, "block")
                    );

                case "DomainBuyEvent":
                    return new DomainBuyEvent
                    (
                        DeserializeEventType(obj, "type"),
                        DeserializeAddress(obj, "sourceAddress"),
                        ParseEventsEventsEdgesNodeBlock(obj, "block")
                    );

                case "DomainClaimEvent":
                    return new DomainClaimEvent
                    (
                        DeserializeEventType(obj, "type"),
                        DeserializeAddress(obj, "sourceAddress"),
                        ParseEventsEventsEdgesNodeBlock(obj, "block")
                    );

                case "DomainCommitEvent":
                    return new DomainCommitEvent
                    (
                        DeserializeEventType(obj, "type"),
                        DeserializeAddress(obj, "sourceAddress"),
                        ParseEventsEventsEdgesNodeBlock(obj, "block")
                    );

                case "DomainGrantEvent":
                    return new DomainGrantEvent
                    (
                        DeserializeEventType(obj, "type"),
                        DeserializeAddress(obj, "sourceAddress"),
                        ParseEventsEventsEdgesNodeBlock(obj, "block")
                    );

                case "DomainRenewEvent":
                    return new DomainRenewEvent
                    (
                        DeserializeEventType(obj, "type"),
                        DeserializeAddress(obj, "sourceAddress"),
                        ParseEventsEventsEdgesNodeBlock(obj, "block")
                    );

                case "DomainSetChildRecordEvent":
                    return new DomainSetChildRecordEvent
                    (
                        DeserializeEventType(obj, "type"),
                        DeserializeAddress(obj, "sourceAddress"),
                        ParseEventsEventsEdgesNodeBlock(obj, "block")
                    );

                case "DomainTransferEvent":
                    return new DomainTransferEvent
                    (
                        DeserializeEventType(obj, "type"),
                        DeserializeAddress(obj, "sourceAddress"),
                        ParseEventsEventsEdgesNodeBlock(obj, "block")
                    );

                case "DomainUpdateEvent":
                    return new DomainUpdateEvent
                    (
                        DeserializeEventType(obj, "type"),
                        DeserializeAddress(obj, "sourceAddress"),
                        ParseEventsEventsEdgesNodeBlock(obj, "block")
                    );

                case "DomainUpdateOperatorsEvent":
                    return new DomainUpdateOperatorsEvent
                    (
                        DeserializeEventType(obj, "type"),
                        DeserializeAddress(obj, "sourceAddress"),
                        ParseEventsEventsEdgesNodeBlock(obj, "block")
                    );

                case "OfferExecutedEvent":
                    return new OfferExecutedEvent
                    (
                        DeserializeEventType(obj, "type"),
                        DeserializeAddress(obj, "sourceAddress"),
                        ParseEventsEventsEdgesNodeBlock(obj, "block"),
                        DeserializeString(obj, "domainName"),
                        DeserializeMutez(obj, "priceWithoutFee"),
                        DeserializeAddress(obj, "sellerAddress")
                    );

                case "OfferPlacedEvent":
                    return new OfferPlacedEvent
                    (
                        DeserializeEventType(obj, "type"),
                        DeserializeAddress(obj, "sourceAddress"),
                        ParseEventsEventsEdgesNodeBlock(obj, "block")
                    );

                case "OfferRemovedEvent":
                    return new OfferRemovedEvent
                    (
                        DeserializeEventType(obj, "type"),
                        DeserializeAddress(obj, "sourceAddress"),
                        ParseEventsEventsEdgesNodeBlock(obj, "block")
                    );

                case "OfferUpdatedEvent":
                    return new OfferUpdatedEvent
                    (
                        DeserializeEventType(obj, "type"),
                        DeserializeAddress(obj, "sourceAddress"),
                        ParseEventsEventsEdgesNodeBlock(obj, "block")
                    );

                case "ReverseRecordClaimEvent":
                    return new ReverseRecordClaimEvent
                    (
                        DeserializeEventType(obj, "type"),
                        DeserializeAddress(obj, "sourceAddress"),
                        ParseEventsEventsEdgesNodeBlock(obj, "block")
                    );

                case "ReverseRecordUpdateEvent":
                    return new ReverseRecordUpdateEvent
                    (
                        DeserializeEventType(obj, "type"),
                        DeserializeAddress(obj, "sourceAddress"),
                        ParseEventsEventsEdgesNodeBlock(obj, "block")
                    );

                default:
                    throw new UnknownSchemaTypeException(type);
            }
        }

        private global::TezosDomains.NotificationService.Services.GraphQL.Generated.IBlock ParseEventsEventsEdgesNodeBlock(
            JsonElement parent,
            string field)
        {
            JsonElement obj = parent.GetProperty(field);

            return new Block
            (
                DeserializeInt(obj, "level"),
                DeserializeDateTime(obj, "timestamp")
            );
        }

        private global::System.Collections.Generic.IReadOnlyList<global::TezosDomains.NotificationService.Services.GraphQL.Generated.IAuctionParticipant> ParseEventsEventsEdgesNodeParticipants(
            JsonElement parent,
            string field)
        {
            JsonElement obj = parent.GetProperty(field);

            int objLength = obj.GetArrayLength();
            var list = new global::TezosDomains.NotificationService.Services.GraphQL.Generated.IAuctionParticipant[objLength];
            for (int objIndex = 0; objIndex < objLength; objIndex++)
            {
                JsonElement element = obj[objIndex];
                list[objIndex] = new AuctionParticipant
                (
                    DeserializeAddress(element, "address")
                );

            }

            return list;
        }

        private string DeserializeNullableString(JsonElement obj, string fieldName)
        {
            if (!obj.TryGetProperty(fieldName, out JsonElement value))
            {
                return null;
            }

            if (value.ValueKind == JsonValueKind.Null)
            {
                return null;
            }

            return (string)_stringSerializer.Deserialize(value.GetString());
        }

        private bool DeserializeBoolean(JsonElement obj, string fieldName)
        {
            JsonElement value = obj.GetProperty(fieldName);
            return (bool)_booleanSerializer.Deserialize(value.GetBoolean());
        }
        private EventType DeserializeEventType(JsonElement obj, string fieldName)
        {
            JsonElement value = obj.GetProperty(fieldName);
            return (EventType)_eventTypeSerializer.Deserialize(value.GetString());
        }

        private string DeserializeAddress(JsonElement obj, string fieldName)
        {
            JsonElement value = obj.GetProperty(fieldName);
            return (string)_addressSerializer.Deserialize(value.GetString());
        }

        private string DeserializeString(JsonElement obj, string fieldName)
        {
            JsonElement value = obj.GetProperty(fieldName);
            return (string)_stringSerializer.Deserialize(value.GetString());
        }

        private decimal DeserializeMutez(JsonElement obj, string fieldName)
        {
            JsonElement value = obj.GetProperty(fieldName);
            return (decimal)_mutezSerializer.Deserialize(value.GetString());
        }

        private string DeserializeNullableAddress(JsonElement obj, string fieldName)
        {
            if (!obj.TryGetProperty(fieldName, out JsonElement value))
            {
                return null;
            }

            if (value.ValueKind == JsonValueKind.Null)
            {
                return null;
            }

            return (string)_addressSerializer.Deserialize(value.GetString());
        }
        private int DeserializeInt(JsonElement obj, string fieldName)
        {
            JsonElement value = obj.GetProperty(fieldName);
            return (int)_intSerializer.Deserialize(value.GetInt32());
        }

        private System.DateTimeOffset DeserializeDateTime(JsonElement obj, string fieldName)
        {
            JsonElement value = obj.GetProperty(fieldName);
            return (System.DateTimeOffset)_dateTimeSerializer.Deserialize(value.GetString());
        }
    }
}
