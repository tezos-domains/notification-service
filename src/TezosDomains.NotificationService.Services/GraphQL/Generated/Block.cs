﻿using System;
using System.Collections;
using System.Collections.Generic;
using StrawberryShake;

namespace TezosDomains.NotificationService.Services.GraphQL.Generated
{
    [System.CodeDom.Compiler.GeneratedCode("StrawberryShake", "11.0.0")]
    public partial class Block
        : IBlock
    {
        public Block(
            int level, 
            System.DateTimeOffset timestamp)
        {
            Level = level;
            Timestamp = timestamp;
        }

        public int Level { get; }

        public System.DateTimeOffset Timestamp { get; }
    }
}
