﻿using System;
using System.Collections;
using System.Collections.Generic;
using StrawberryShake;

namespace TezosDomains.NotificationService.Services.GraphQL.Generated
{
    [System.CodeDom.Compiler.GeneratedCode("StrawberryShake", "11.0.0")]
    public partial class DomainsOperation
        : IOperation<IDomains>
    {
        public string Name => "Domains";

        public IDocument Document => Queries.Default;

        public OperationKind Kind => OperationKind.Query;

        public Type ResultType => typeof(IDomains);

        public Optional<int?> First { get; set; }

        public Optional<string> After { get; set; }

        public Optional<global::TezosDomains.NotificationService.Services.GraphQL.Generated.DomainsFilter> Where { get; set; }

        public Optional<global::TezosDomains.NotificationService.Services.GraphQL.Generated.DomainOrder> Order { get; set; }

        public IReadOnlyList<VariableValue> GetVariableValues()
        {
            var variables = new List<VariableValue>();

            if (First.HasValue)
            {
                variables.Add(new VariableValue("first", "Int", First.Value));
            }

            if (After.HasValue)
            {
                variables.Add(new VariableValue("after", "String", After.Value));
            }

            if (Where.HasValue)
            {
                variables.Add(new VariableValue("where", "DomainsFilter", Where.Value));
            }

            if (Order.HasValue)
            {
                variables.Add(new VariableValue("order", "DomainOrder", Order.Value));
            }

            return variables;
        }
    }
}
