﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using StrawberryShake;

namespace TezosDomains.NotificationService.Services.GraphQL.Generated
{
    [System.CodeDom.Compiler.GeneratedCode("StrawberryShake", "11.0.0")]
    public partial interface ITezosDomainsClient
    {
        Task<IOperationResult<global::TezosDomains.NotificationService.Services.GraphQL.Generated.IEvents>> EventsAsync(
            Optional<int?> first = default,
            Optional<string> after = default,
            Optional<global::TezosDomains.NotificationService.Services.GraphQL.Generated.EventsFilter> where = default,
            Optional<global::TezosDomains.NotificationService.Services.GraphQL.Generated.EventOrder> order = default,
            CancellationToken cancellationToken = default);

        Task<IOperationResult<global::TezosDomains.NotificationService.Services.GraphQL.Generated.IEvents>> EventsAsync(
            EventsOperation operation,
            CancellationToken cancellationToken = default);

        Task<IOperationResult<global::TezosDomains.NotificationService.Services.GraphQL.Generated.IDomains>> DomainsAsync(
            Optional<int?> first = default,
            Optional<string> after = default,
            Optional<global::TezosDomains.NotificationService.Services.GraphQL.Generated.DomainsFilter> where = default,
            Optional<global::TezosDomains.NotificationService.Services.GraphQL.Generated.DomainOrder> order = default,
            CancellationToken cancellationToken = default);

        Task<IOperationResult<global::TezosDomains.NotificationService.Services.GraphQL.Generated.IDomains>> DomainsAsync(
            DomainsOperation operation,
            CancellationToken cancellationToken = default);
    }
}
