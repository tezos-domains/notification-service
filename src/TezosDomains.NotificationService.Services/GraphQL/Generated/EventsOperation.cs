﻿using System;
using System.Collections;
using System.Collections.Generic;
using StrawberryShake;

namespace TezosDomains.NotificationService.Services.GraphQL.Generated
{
    [System.CodeDom.Compiler.GeneratedCode("StrawberryShake", "11.0.0")]
    public partial class EventsOperation
        : IOperation<IEvents>
    {
        public string Name => "Events";

        public IDocument Document => Queries.Default;

        public OperationKind Kind => OperationKind.Query;

        public Type ResultType => typeof(IEvents);

        public Optional<int?> First { get; set; }

        public Optional<string> After { get; set; }

        public Optional<global::TezosDomains.NotificationService.Services.GraphQL.Generated.EventsFilter> Where { get; set; }

        public Optional<global::TezosDomains.NotificationService.Services.GraphQL.Generated.EventOrder> Order { get; set; }

        public IReadOnlyList<VariableValue> GetVariableValues()
        {
            var variables = new List<VariableValue>();

            if (First.HasValue)
            {
                variables.Add(new VariableValue("first", "Int", First.Value));
            }

            if (After.HasValue)
            {
                variables.Add(new VariableValue("after", "String", After.Value));
            }

            if (Where.HasValue)
            {
                variables.Add(new VariableValue("where", "EventsFilter", Where.Value));
            }

            if (Order.HasValue)
            {
                variables.Add(new VariableValue("order", "EventOrder", Order.Value));
            }

            return variables;
        }
    }
}
