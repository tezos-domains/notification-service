﻿using System;
using System.Collections;
using System.Collections.Generic;
using StrawberryShake;

namespace TezosDomains.NotificationService.Services.GraphQL.Generated
{
    [System.CodeDom.Compiler.GeneratedCode("StrawberryShake", "11.0.0")]
    public partial class RecordValidityValueSerializer
        : IValueSerializer
    {
        public string Name => "RecordValidity";

        public ValueKind Kind => ValueKind.Enum;

        public Type ClrType => typeof(RecordValidity);

        public Type SerializationType => typeof(string);

        public object Serialize(object value)
        {
            if (value is null)
            {
                return null;
            }

            var enumValue = (RecordValidity)value;

            switch(enumValue)
            {
                case RecordValidity.Valid:
                    return "VALID";
                case RecordValidity.Expired:
                    return "EXPIRED";
                case RecordValidity.All:
                    return "ALL";
                default:
                    throw new NotSupportedException();
            }
        }

        public object Deserialize(object serialized)
        {
            if (serialized is null)
            {
                return null;
            }

            var stringValue = (string)serialized;

            switch(stringValue)
            {
                case "VALID":
                    return RecordValidity.Valid;
                case "EXPIRED":
                    return RecordValidity.Expired;
                case "ALL":
                    return RecordValidity.All;
                default:
                    throw new NotSupportedException();
            }
        }

    }
}
