﻿using System;
using System.Collections;
using System.Collections.Generic;
using StrawberryShake;

namespace TezosDomains.NotificationService.Services.GraphQL.Generated
{
    [System.CodeDom.Compiler.GeneratedCode("StrawberryShake", "11.0.0")]
    public partial class EventTypeValueSerializer
        : IValueSerializer
    {
        public string Name => "EventType";

        public ValueKind Kind => ValueKind.Enum;

        public Type ClrType => typeof(EventType);

        public Type SerializationType => typeof(string);

        public object Serialize(object value)
        {
            if (value is null)
            {
                return null;
            }

            var enumValue = (EventType)value;

            switch(enumValue)
            {
                case EventType.AuctionBidEvent:
                    return "AUCTION_BID_EVENT";
                case EventType.AuctionEndEvent:
                    return "AUCTION_END_EVENT";
                case EventType.AuctionSettleEvent:
                    return "AUCTION_SETTLE_EVENT";
                case EventType.AuctionWithdrawEvent:
                    return "AUCTION_WITHDRAW_EVENT";
                case EventType.BuyOfferExecutedEvent:
                    return "BUY_OFFER_EXECUTED_EVENT";
                case EventType.BuyOfferPlacedEvent:
                    return "BUY_OFFER_PLACED_EVENT";
                case EventType.BuyOfferRemovedEvent:
                    return "BUY_OFFER_REMOVED_EVENT";
                case EventType.DomainBuyEvent:
                    return "DOMAIN_BUY_EVENT";
                case EventType.DomainClaimEvent:
                    return "DOMAIN_CLAIM_EVENT";
                case EventType.DomainCommitEvent:
                    return "DOMAIN_COMMIT_EVENT";
                case EventType.DomainGrantEvent:
                    return "DOMAIN_GRANT_EVENT";
                case EventType.DomainRenewEvent:
                    return "DOMAIN_RENEW_EVENT";
                case EventType.DomainSetChildRecordEvent:
                    return "DOMAIN_SET_CHILD_RECORD_EVENT";
                case EventType.DomainTransferEvent:
                    return "DOMAIN_TRANSFER_EVENT";
                case EventType.DomainUpdateEvent:
                    return "DOMAIN_UPDATE_EVENT";
                case EventType.DomainUpdateOperatorsEvent:
                    return "DOMAIN_UPDATE_OPERATORS_EVENT";
                case EventType.OfferExecutedEvent:
                    return "OFFER_EXECUTED_EVENT";
                case EventType.OfferPlacedEvent:
                    return "OFFER_PLACED_EVENT";
                case EventType.OfferRemovedEvent:
                    return "OFFER_REMOVED_EVENT";
                case EventType.OfferUpdatedEvent:
                    return "OFFER_UPDATED_EVENT";
                case EventType.ReverseRecordClaimEvent:
                    return "REVERSE_RECORD_CLAIM_EVENT";
                case EventType.ReverseRecordUpdateEvent:
                    return "REVERSE_RECORD_UPDATE_EVENT";
                default:
                    throw new NotSupportedException();
            }
        }

        public object Deserialize(object serialized)
        {
            if (serialized is null)
            {
                return null;
            }

            var stringValue = (string)serialized;

            switch(stringValue)
            {
                case "AUCTION_BID_EVENT":
                    return EventType.AuctionBidEvent;
                case "AUCTION_END_EVENT":
                    return EventType.AuctionEndEvent;
                case "AUCTION_SETTLE_EVENT":
                    return EventType.AuctionSettleEvent;
                case "AUCTION_WITHDRAW_EVENT":
                    return EventType.AuctionWithdrawEvent;
                case "BUY_OFFER_EXECUTED_EVENT":
                    return EventType.BuyOfferExecutedEvent;
                case "BUY_OFFER_PLACED_EVENT":
                    return EventType.BuyOfferPlacedEvent;
                case "BUY_OFFER_REMOVED_EVENT":
                    return EventType.BuyOfferRemovedEvent;
                case "DOMAIN_BUY_EVENT":
                    return EventType.DomainBuyEvent;
                case "DOMAIN_CLAIM_EVENT":
                    return EventType.DomainClaimEvent;
                case "DOMAIN_COMMIT_EVENT":
                    return EventType.DomainCommitEvent;
                case "DOMAIN_GRANT_EVENT":
                    return EventType.DomainGrantEvent;
                case "DOMAIN_RENEW_EVENT":
                    return EventType.DomainRenewEvent;
                case "DOMAIN_SET_CHILD_RECORD_EVENT":
                    return EventType.DomainSetChildRecordEvent;
                case "DOMAIN_TRANSFER_EVENT":
                    return EventType.DomainTransferEvent;
                case "DOMAIN_UPDATE_EVENT":
                    return EventType.DomainUpdateEvent;
                case "DOMAIN_UPDATE_OPERATORS_EVENT":
                    return EventType.DomainUpdateOperatorsEvent;
                case "OFFER_EXECUTED_EVENT":
                    return EventType.OfferExecutedEvent;
                case "OFFER_PLACED_EVENT":
                    return EventType.OfferPlacedEvent;
                case "OFFER_REMOVED_EVENT":
                    return EventType.OfferRemovedEvent;
                case "OFFER_UPDATED_EVENT":
                    return EventType.OfferUpdatedEvent;
                case "REVERSE_RECORD_CLAIM_EVENT":
                    return EventType.ReverseRecordClaimEvent;
                case "REVERSE_RECORD_UPDATE_EVENT":
                    return EventType.ReverseRecordUpdateEvent;
                default:
                    throw new NotSupportedException();
            }
        }

    }
}
