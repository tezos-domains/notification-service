﻿using System;
using System.Collections;
using System.Collections.Generic;
using StrawberryShake;

namespace TezosDomains.NotificationService.Services.GraphQL.Generated
{
    [System.CodeDom.Compiler.GeneratedCode("StrawberryShake", "11.0.0")]
    public partial class EventOrderSerializer
        : IInputSerializer
    {
        private bool _needsInitialization = true;
        private IValueSerializer _orderDirectionSerializer;
        private IValueSerializer _eventOrderFieldSerializer;

        public string Name { get; } = "EventOrder";

        public ValueKind Kind { get; } = ValueKind.InputObject;

        public Type ClrType => typeof(EventOrder);

        public Type SerializationType => typeof(IReadOnlyDictionary<string, object>);

        public void Initialize(IValueSerializerCollection serializerResolver)
        {
            if (serializerResolver is null)
            {
                throw new ArgumentNullException(nameof(serializerResolver));
            }
            _orderDirectionSerializer = serializerResolver.Get("OrderDirection");
            _eventOrderFieldSerializer = serializerResolver.Get("EventOrderField");
            _needsInitialization = false;
        }

        public object Serialize(object value)
        {
            if (_needsInitialization)
            {
                throw new InvalidOperationException(
                    $"The serializer for type `{Name}` has not been initialized.");
            }

            if (value is null)
            {
                return null;
            }

            var input = (EventOrder)value;
            var map = new Dictionary<string, object>();

            if (input.Direction.HasValue)
            {
                map.Add("direction", SerializeNullableOrderDirection(input.Direction.Value));
            }

            if (input.Field.HasValue)
            {
                map.Add("field", SerializeNullableEventOrderField(input.Field.Value));
            }

            return map;
        }

        private object SerializeNullableOrderDirection(object value)
        {
            if (value is null)
            {
                return null;
            }


            return _orderDirectionSerializer.Serialize(value);
        }
        private object SerializeNullableEventOrderField(object value)
        {
            return _eventOrderFieldSerializer.Serialize(value);
        }

        public object Deserialize(object value)
        {
            throw new NotSupportedException(
                "Deserializing input values is not supported.");
        }
    }
}
