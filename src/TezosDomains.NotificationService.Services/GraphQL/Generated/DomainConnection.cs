﻿using System;
using System.Collections;
using System.Collections.Generic;
using StrawberryShake;

namespace TezosDomains.NotificationService.Services.GraphQL.Generated
{
    [System.CodeDom.Compiler.GeneratedCode("StrawberryShake", "11.0.0")]
    public partial class DomainConnection
        : IDomainConnection
    {
        public DomainConnection(
            global::System.Collections.Generic.IReadOnlyList<global::TezosDomains.NotificationService.Services.GraphQL.Generated.IDomainEdge> edges, 
            global::TezosDomains.NotificationService.Services.GraphQL.Generated.IPageInfo1 pageInfo)
        {
            Edges = edges;
            PageInfo = pageInfo;
        }

        public global::System.Collections.Generic.IReadOnlyList<global::TezosDomains.NotificationService.Services.GraphQL.Generated.IDomainEdge> Edges { get; }

        public global::TezosDomains.NotificationService.Services.GraphQL.Generated.IPageInfo1 PageInfo { get; }
    }
}
