﻿using System;
using System.Collections;
using System.Collections.Generic;
using StrawberryShake;

namespace TezosDomains.NotificationService.Services.GraphQL.Generated
{
    [System.CodeDom.Compiler.GeneratedCode("StrawberryShake", "11.0.0")]
    public partial class PageInfo
        : IPageInfo
    {
        public PageInfo(
            string endCursor, 
            bool hasNextPage)
        {
            EndCursor = endCursor;
            HasNextPage = hasNextPage;
        }

        public string EndCursor { get; }

        public bool HasNextPage { get; }
    }
}
