﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.Json;
using StrawberryShake;
using StrawberryShake.Configuration;
using StrawberryShake.Http;
using StrawberryShake.Http.Subscriptions;
using StrawberryShake.Transport;

namespace TezosDomains.NotificationService.Services.GraphQL.Generated
{
    [System.CodeDom.Compiler.GeneratedCode("StrawberryShake", "11.0.0")]
    public partial class DomainsResultParser
        : JsonResultParserBase<IDomains>
    {
        private readonly IValueSerializer _stringSerializer;
        private readonly IValueSerializer _booleanSerializer;
        private readonly IValueSerializer _dateTimeSerializer;
        private readonly IValueSerializer _addressSerializer;

        public DomainsResultParser(IValueSerializerCollection serializerResolver)
        {
            if (serializerResolver is null)
            {
                throw new ArgumentNullException(nameof(serializerResolver));
            }
            _stringSerializer = serializerResolver.Get("String");
            _booleanSerializer = serializerResolver.Get("Boolean");
            _dateTimeSerializer = serializerResolver.Get("DateTime");
            _addressSerializer = serializerResolver.Get("Address");
        }

        protected override IDomains ParserData(JsonElement data)
        {
            return new Domains1
            (
                ParseDomainsDomains(data, "domains")
            );

        }

        private global::TezosDomains.NotificationService.Services.GraphQL.Generated.IDomainConnection ParseDomainsDomains(
            JsonElement parent,
            string field)
        {
            JsonElement obj = parent.GetProperty(field);

            return new DomainConnection
            (
                ParseDomainsDomainsEdges(obj, "edges"),
                ParseDomainsDomainsPageInfo(obj, "pageInfo")
            );
        }

        private global::System.Collections.Generic.IReadOnlyList<global::TezosDomains.NotificationService.Services.GraphQL.Generated.IDomainEdge> ParseDomainsDomainsEdges(
            JsonElement parent,
            string field)
        {
            JsonElement obj = parent.GetProperty(field);

            int objLength = obj.GetArrayLength();
            var list = new global::TezosDomains.NotificationService.Services.GraphQL.Generated.IDomainEdge[objLength];
            for (int objIndex = 0; objIndex < objLength; objIndex++)
            {
                JsonElement element = obj[objIndex];
                list[objIndex] = new DomainEdge
                (
                    ParseDomainsDomainsEdgesNode(element, "node")
                );

            }

            return list;
        }

        private global::TezosDomains.NotificationService.Services.GraphQL.Generated.IPageInfo1 ParseDomainsDomainsPageInfo(
            JsonElement parent,
            string field)
        {
            JsonElement obj = parent.GetProperty(field);

            return new PageInfo1
            (
                DeserializeNullableString(obj, "endCursor"),
                DeserializeBoolean(obj, "hasNextPage")
            );
        }

        private global::TezosDomains.NotificationService.Services.GraphQL.Generated.IDomain ParseDomainsDomainsEdgesNode(
            JsonElement parent,
            string field)
        {
            JsonElement obj = parent.GetProperty(field);

            return new Domain
            (
                DeserializeString(obj, "name"),
                DeserializeNullableDateTime(obj, "expiresAtUtc"),
                DeserializeAddress(obj, "owner")
            );
        }

        private string DeserializeNullableString(JsonElement obj, string fieldName)
        {
            if (!obj.TryGetProperty(fieldName, out JsonElement value))
            {
                return null;
            }

            if (value.ValueKind == JsonValueKind.Null)
            {
                return null;
            }

            return (string)_stringSerializer.Deserialize(value.GetString());
        }

        private bool DeserializeBoolean(JsonElement obj, string fieldName)
        {
            JsonElement value = obj.GetProperty(fieldName);
            return (bool)_booleanSerializer.Deserialize(value.GetBoolean());
        }
        private string DeserializeString(JsonElement obj, string fieldName)
        {
            JsonElement value = obj.GetProperty(fieldName);
            return (string)_stringSerializer.Deserialize(value.GetString());
        }

        private System.DateTimeOffset? DeserializeNullableDateTime(JsonElement obj, string fieldName)
        {
            if (!obj.TryGetProperty(fieldName, out JsonElement value))
            {
                return null;
            }

            if (value.ValueKind == JsonValueKind.Null)
            {
                return null;
            }

            return (System.DateTimeOffset?)_dateTimeSerializer.Deserialize(value.GetString());
        }

        private string DeserializeAddress(JsonElement obj, string fieldName)
        {
            JsonElement value = obj.GetProperty(fieldName);
            return (string)_addressSerializer.Deserialize(value.GetString());
        }
    }
}
