﻿using System;
using System.Collections;
using System.Collections.Generic;
using StrawberryShake;

namespace TezosDomains.NotificationService.Services.GraphQL.Generated
{
    [System.CodeDom.Compiler.GeneratedCode("StrawberryShake", "11.0.0")]
    public partial class MutezFilter
    {
        public Optional<decimal?> EqualTo { get; set; }

        public Optional<decimal?> GreaterThan { get; set; }

        public Optional<decimal?> GreaterThanOrEqualTo { get; set; }

        public Optional<IReadOnlyList<decimal>> In { get; set; }

        public Optional<decimal?> LessThan { get; set; }

        public Optional<decimal?> LessThanOrEqualTo { get; set; }

        public Optional<decimal?> NotEqualTo { get; set; }

        public Optional<IReadOnlyList<decimal>> NotIn { get; set; }
    }
}
