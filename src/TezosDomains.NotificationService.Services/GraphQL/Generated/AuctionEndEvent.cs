﻿using System;
using System.Collections;
using System.Collections.Generic;
using StrawberryShake;

namespace TezosDomains.NotificationService.Services.GraphQL.Generated
{
    [System.CodeDom.Compiler.GeneratedCode("StrawberryShake", "11.0.0")]
    public partial class AuctionEndEvent
        : IEvent
        , IAuctionEndEvent1
    {
        public AuctionEndEvent(
            EventType type, 
            string sourceAddress, 
            global::TezosDomains.NotificationService.Services.GraphQL.Generated.IBlock block, 
            string domainName, 
            global::System.Collections.Generic.IReadOnlyList<global::TezosDomains.NotificationService.Services.GraphQL.Generated.IAuctionParticipant> participants)
        {
            Type = type;
            SourceAddress = sourceAddress;
            Block = block;
            DomainName = domainName;
            Participants = participants;
        }

        public EventType Type { get; }

        public string SourceAddress { get; }

        public global::TezosDomains.NotificationService.Services.GraphQL.Generated.IBlock Block { get; }

        public string DomainName { get; }

        public global::System.Collections.Generic.IReadOnlyList<global::TezosDomains.NotificationService.Services.GraphQL.Generated.IAuctionParticipant> Participants { get; }
    }
}
