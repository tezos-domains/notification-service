﻿using System;
using System.Collections;
using System.Collections.Generic;
using StrawberryShake;

namespace TezosDomains.NotificationService.Services.GraphQL.Generated
{
    [System.CodeDom.Compiler.GeneratedCode("StrawberryShake", "11.0.0")]
    public partial class EventEdge
        : IEventEdge
    {
        public EventEdge(
            global::TezosDomains.NotificationService.Services.GraphQL.Generated.IEvent node)
        {
            Node = node;
        }

        public global::TezosDomains.NotificationService.Services.GraphQL.Generated.IEvent Node { get; }
    }
}
