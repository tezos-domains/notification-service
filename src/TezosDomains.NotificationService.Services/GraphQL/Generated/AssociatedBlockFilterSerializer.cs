﻿using System;
using System.Collections;
using System.Collections.Generic;
using StrawberryShake;

namespace TezosDomains.NotificationService.Services.GraphQL.Generated
{
    [System.CodeDom.Compiler.GeneratedCode("StrawberryShake", "11.0.0")]
    public partial class AssociatedBlockFilterSerializer
        : IInputSerializer
    {
        private bool _needsInitialization = true;
        private IValueSerializer _stringEqualityFilterSerializer;
        private IValueSerializer _intFilterSerializer;
        private IValueSerializer _dateTimeFilterSerializer;

        public string Name { get; } = "AssociatedBlockFilter";

        public ValueKind Kind { get; } = ValueKind.InputObject;

        public Type ClrType => typeof(AssociatedBlockFilter);

        public Type SerializationType => typeof(IReadOnlyDictionary<string, object>);

        public void Initialize(IValueSerializerCollection serializerResolver)
        {
            if (serializerResolver is null)
            {
                throw new ArgumentNullException(nameof(serializerResolver));
            }
            _stringEqualityFilterSerializer = serializerResolver.Get("StringEqualityFilter");
            _intFilterSerializer = serializerResolver.Get("IntFilter");
            _dateTimeFilterSerializer = serializerResolver.Get("DateTimeFilter");
            _needsInitialization = false;
        }

        public object Serialize(object value)
        {
            if (_needsInitialization)
            {
                throw new InvalidOperationException(
                    $"The serializer for type `{Name}` has not been initialized.");
            }

            if (value is null)
            {
                return null;
            }

            var input = (AssociatedBlockFilter)value;
            var map = new Dictionary<string, object>();

            if (input.Hash.HasValue)
            {
                map.Add("hash", SerializeNullableStringEqualityFilter(input.Hash.Value));
            }

            if (input.Level.HasValue)
            {
                map.Add("level", SerializeNullableIntFilter(input.Level.Value));
            }

            if (input.Timestamp.HasValue)
            {
                map.Add("timestamp", SerializeNullableDateTimeFilter(input.Timestamp.Value));
            }

            return map;
        }

        private object SerializeNullableStringEqualityFilter(object value)
        {
            if (value is null)
            {
                return null;
            }


            return _stringEqualityFilterSerializer.Serialize(value);
        }
        private object SerializeNullableIntFilter(object value)
        {
            if (value is null)
            {
                return null;
            }


            return _intFilterSerializer.Serialize(value);
        }
        private object SerializeNullableDateTimeFilter(object value)
        {
            if (value is null)
            {
                return null;
            }


            return _dateTimeFilterSerializer.Serialize(value);
        }

        public object Deserialize(object value)
        {
            throw new NotSupportedException(
                "Deserializing input values is not supported.");
        }
    }
}
