﻿using System;
using System.Collections;
using System.Collections.Generic;
using StrawberryShake;

namespace TezosDomains.NotificationService.Services.GraphQL.Generated
{
    [System.CodeDom.Compiler.GeneratedCode("StrawberryShake", "11.0.0")]
    public partial class AddressFilter
    {
        public Optional<string> EqualTo { get; set; }

        public Optional<IReadOnlyList<string>> In { get; set; }

        public Optional<string> NotEqualTo { get; set; }

        public Optional<IReadOnlyList<string>> NotIn { get; set; }

        public Optional<AddressPrefix?> StartsWith { get; set; }
    }
}
