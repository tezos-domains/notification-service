﻿using System;
using System.Collections;
using System.Collections.Generic;
using StrawberryShake;

namespace TezosDomains.NotificationService.Services.GraphQL.Generated
{
    [System.CodeDom.Compiler.GeneratedCode("StrawberryShake", "11.0.0")]
    public partial class EventsFilter
    {
        public Optional<global::TezosDomains.NotificationService.Services.GraphQL.Generated.AddressFilter> Address { get; set; }

        public Optional<global::System.Collections.Generic.List<global::TezosDomains.NotificationService.Services.GraphQL.Generated.EventsFilter>> And { get; set; }

        public Optional<global::TezosDomains.NotificationService.Services.GraphQL.Generated.AssociatedBlockFilter> Block { get; set; }

        public Optional<global::TezosDomains.NotificationService.Services.GraphQL.Generated.StringFilter> DomainName { get; set; }

        public Optional<global::System.Collections.Generic.List<global::TezosDomains.NotificationService.Services.GraphQL.Generated.EventsFilter>> Or { get; set; }

        public Optional<global::TezosDomains.NotificationService.Services.GraphQL.Generated.MutezFilter> Price { get; set; }

        public Optional<global::TezosDomains.NotificationService.Services.GraphQL.Generated.EventTypeFilter> Type { get; set; }
    }
}
