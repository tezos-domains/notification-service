﻿using System;
using System.Collections;
using System.Collections.Generic;
using StrawberryShake;

namespace TezosDomains.NotificationService.Services.GraphQL.Generated
{
    [System.CodeDom.Compiler.GeneratedCode("StrawberryShake", "11.0.0")]
    public partial class AuctionParticipant
        : IAuctionParticipant
    {
        public AuctionParticipant(
            string address)
        {
            Address = address;
        }

        public string Address { get; }
    }
}
