﻿using System;
using System.Collections;
using System.Collections.Generic;
using StrawberryShake;

namespace TezosDomains.NotificationService.Services.GraphQL.Generated
{
    [System.CodeDom.Compiler.GeneratedCode("StrawberryShake", "11.0.0")]
    public partial class EventsFilterSerializer
        : IInputSerializer
    {
        private bool _needsInitialization = true;
        private IValueSerializer _addressFilterSerializer;
        private IValueSerializer _eventsFilterSerializer;
        private IValueSerializer _associatedBlockFilterSerializer;
        private IValueSerializer _stringFilterSerializer;
        private IValueSerializer _mutezFilterSerializer;
        private IValueSerializer _eventTypeFilterSerializer;

        public string Name { get; } = "EventsFilter";

        public ValueKind Kind { get; } = ValueKind.InputObject;

        public Type ClrType => typeof(EventsFilter);

        public Type SerializationType => typeof(IReadOnlyDictionary<string, object>);

        public void Initialize(IValueSerializerCollection serializerResolver)
        {
            if (serializerResolver is null)
            {
                throw new ArgumentNullException(nameof(serializerResolver));
            }
            _addressFilterSerializer = serializerResolver.Get("AddressFilter");
            _eventsFilterSerializer = serializerResolver.Get("EventsFilter");
            _associatedBlockFilterSerializer = serializerResolver.Get("AssociatedBlockFilter");
            _stringFilterSerializer = serializerResolver.Get("StringFilter");
            _mutezFilterSerializer = serializerResolver.Get("MutezFilter");
            _eventTypeFilterSerializer = serializerResolver.Get("EventTypeFilter");
            _needsInitialization = false;
        }

        public object Serialize(object value)
        {
            if (_needsInitialization)
            {
                throw new InvalidOperationException(
                    $"The serializer for type `{Name}` has not been initialized.");
            }

            if (value is null)
            {
                return null;
            }

            var input = (EventsFilter)value;
            var map = new Dictionary<string, object>();

            if (input.Address.HasValue)
            {
                map.Add("address", SerializeNullableAddressFilter(input.Address.Value));
            }

            if (input.And.HasValue)
            {
                map.Add("and", SerializeNullableListOfEventsFilter(input.And.Value));
            }

            if (input.Block.HasValue)
            {
                map.Add("block", SerializeNullableAssociatedBlockFilter(input.Block.Value));
            }

            if (input.DomainName.HasValue)
            {
                map.Add("domainName", SerializeNullableStringFilter(input.DomainName.Value));
            }

            if (input.Or.HasValue)
            {
                map.Add("or", SerializeNullableListOfEventsFilter(input.Or.Value));
            }

            if (input.Price.HasValue)
            {
                map.Add("price", SerializeNullableMutezFilter(input.Price.Value));
            }

            if (input.Type.HasValue)
            {
                map.Add("type", SerializeNullableEventTypeFilter(input.Type.Value));
            }

            return map;
        }

        private object SerializeNullableAddressFilter(object value)
        {
            if (value is null)
            {
                return null;
            }


            return _addressFilterSerializer.Serialize(value);
        }
        private object SerializeNullableEventsFilter(object value)
        {
            return _eventsFilterSerializer.Serialize(value);
        }

        private object SerializeNullableListOfEventsFilter(object value)
        {
            if (value is null)
            {
                return null;
            }


            IList source = (IList)value;
            object[] result = new object[source.Count];
            for(int i = 0; i < source.Count; i++)
            {
                result[i] = SerializeNullableEventsFilter(source[i]);
            }
            return result;
        }
        private object SerializeNullableAssociatedBlockFilter(object value)
        {
            if (value is null)
            {
                return null;
            }


            return _associatedBlockFilterSerializer.Serialize(value);
        }
        private object SerializeNullableStringFilter(object value)
        {
            if (value is null)
            {
                return null;
            }


            return _stringFilterSerializer.Serialize(value);
        }
        private object SerializeNullableMutezFilter(object value)
        {
            if (value is null)
            {
                return null;
            }


            return _mutezFilterSerializer.Serialize(value);
        }
        private object SerializeNullableEventTypeFilter(object value)
        {
            if (value is null)
            {
                return null;
            }


            return _eventTypeFilterSerializer.Serialize(value);
        }

        public object Deserialize(object value)
        {
            throw new NotSupportedException(
                "Deserializing input values is not supported.");
        }
    }
}
