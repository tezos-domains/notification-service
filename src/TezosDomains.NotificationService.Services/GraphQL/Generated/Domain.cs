﻿using System;
using System.Collections;
using System.Collections.Generic;
using StrawberryShake;

namespace TezosDomains.NotificationService.Services.GraphQL.Generated
{
    [System.CodeDom.Compiler.GeneratedCode("StrawberryShake", "11.0.0")]
    public partial class Domain
        : IDomain
    {
        public Domain(
            string name, 
            System.DateTimeOffset? expiresAtUtc, 
            string owner)
        {
            Name = name;
            ExpiresAtUtc = expiresAtUtc;
            Owner = owner;
        }

        public string Name { get; }

        public System.DateTimeOffset? ExpiresAtUtc { get; }

        public string Owner { get; }
    }
}
