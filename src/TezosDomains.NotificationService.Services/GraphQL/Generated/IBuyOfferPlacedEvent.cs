﻿using System;
using System.Collections;
using System.Collections.Generic;
using StrawberryShake;

namespace TezosDomains.NotificationService.Services.GraphQL.Generated
{
    [System.CodeDom.Compiler.GeneratedCode("StrawberryShake", "11.0.0")]
    public partial interface IBuyOfferPlacedEvent
    {
        string DomainName { get; }

        string DomainOwner { get; }

        decimal PriceWithoutFee { get; }

        string SourceAddress { get; }
    }
}
