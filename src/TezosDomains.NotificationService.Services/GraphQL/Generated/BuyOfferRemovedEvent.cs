﻿using System;
using System.Collections;
using System.Collections.Generic;
using StrawberryShake;

namespace TezosDomains.NotificationService.Services.GraphQL.Generated
{
    [System.CodeDom.Compiler.GeneratedCode("StrawberryShake", "11.0.0")]
    public partial class BuyOfferRemovedEvent
        : IEvent
        , IBuyOfferRemovedEvent
    {
        public BuyOfferRemovedEvent(
            EventType type, 
            string sourceAddress, 
            global::TezosDomains.NotificationService.Services.GraphQL.Generated.IBlock block)
        {
            Type = type;
            SourceAddress = sourceAddress;
            Block = block;
        }

        public EventType Type { get; }

        public string SourceAddress { get; }

        public global::TezosDomains.NotificationService.Services.GraphQL.Generated.IBlock Block { get; }
    }
}
