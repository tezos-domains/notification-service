﻿using System;
using System.Collections;
using System.Collections.Generic;
using StrawberryShake;

namespace TezosDomains.NotificationService.Services.GraphQL.Generated
{
    [System.CodeDom.Compiler.GeneratedCode("StrawberryShake", "11.0.0")]
    public partial class DateTimeFilter
    {
        public Optional<System.DateTimeOffset?> EqualTo { get; set; }

        public Optional<System.DateTimeOffset?> GreaterThan { get; set; }

        public Optional<System.DateTimeOffset?> GreaterThanOrEqualTo { get; set; }

        public Optional<IReadOnlyList<System.DateTimeOffset>> In { get; set; }

        public Optional<System.DateTimeOffset?> LessThan { get; set; }

        public Optional<System.DateTimeOffset?> LessThanOrEqualTo { get; set; }

        public Optional<System.DateTimeOffset?> NotEqualTo { get; set; }

        public Optional<IReadOnlyList<System.DateTimeOffset>> NotIn { get; set; }
    }
}
