﻿using System;
using System.Collections;
using System.Collections.Generic;
using StrawberryShake;

namespace TezosDomains.NotificationService.Services.GraphQL.Generated
{
    [System.CodeDom.Compiler.GeneratedCode("StrawberryShake", "11.0.0")]
    public enum AddressPrefix
    {
        Kt1,
        Txr1,
        Tz1,
        Tz2,
        Tz3
    }
}
