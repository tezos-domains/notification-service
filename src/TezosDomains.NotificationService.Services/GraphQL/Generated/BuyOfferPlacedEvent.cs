﻿using System;
using System.Collections;
using System.Collections.Generic;
using StrawberryShake;

namespace TezosDomains.NotificationService.Services.GraphQL.Generated
{
    [System.CodeDom.Compiler.GeneratedCode("StrawberryShake", "11.0.0")]
    public partial class BuyOfferPlacedEvent
        : IEvent
        , IBuyOfferPlacedEvent1
    {
        public BuyOfferPlacedEvent(
            EventType type, 
            string sourceAddress, 
            global::TezosDomains.NotificationService.Services.GraphQL.Generated.IBlock block, 
            string domainName, 
            string domainOwner, 
            decimal priceWithoutFee)
        {
            Type = type;
            SourceAddress = sourceAddress;
            Block = block;
            DomainName = domainName;
            DomainOwner = domainOwner;
            PriceWithoutFee = priceWithoutFee;
        }

        public EventType Type { get; }

        public string SourceAddress { get; }

        public global::TezosDomains.NotificationService.Services.GraphQL.Generated.IBlock Block { get; }

        public string DomainName { get; }

        public string DomainOwner { get; }

        public decimal PriceWithoutFee { get; }
    }
}
