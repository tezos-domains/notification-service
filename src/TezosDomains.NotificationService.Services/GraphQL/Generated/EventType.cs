﻿using System;
using System.Collections;
using System.Collections.Generic;
using StrawberryShake;

namespace TezosDomains.NotificationService.Services.GraphQL.Generated
{
    [System.CodeDom.Compiler.GeneratedCode("StrawberryShake", "11.0.0")]
    public enum EventType
    {
        AuctionBidEvent,
        AuctionEndEvent,
        AuctionSettleEvent,
        AuctionWithdrawEvent,
        BuyOfferExecutedEvent,
        BuyOfferPlacedEvent,
        BuyOfferRemovedEvent,
        DomainBuyEvent,
        DomainClaimEvent,
        DomainCommitEvent,
        DomainGrantEvent,
        DomainRenewEvent,
        DomainSetChildRecordEvent,
        DomainTransferEvent,
        DomainUpdateEvent,
        DomainUpdateOperatorsEvent,
        OfferExecutedEvent,
        OfferPlacedEvent,
        OfferRemovedEvent,
        OfferUpdatedEvent,
        ReverseRecordClaimEvent,
        ReverseRecordUpdateEvent
    }
}
