﻿using System;
using System.Collections;
using System.Collections.Generic;
using StrawberryShake;

namespace TezosDomains.NotificationService.Services.GraphQL.Generated
{
    [System.CodeDom.Compiler.GeneratedCode("StrawberryShake", "11.0.0")]
    public partial interface IBuyOfferPlacedEvent1
        : IBuyOfferPlacedEvent
    {
        EventType Type { get; }

        global::TezosDomains.NotificationService.Services.GraphQL.Generated.IBlock Block { get; }
    }
}
