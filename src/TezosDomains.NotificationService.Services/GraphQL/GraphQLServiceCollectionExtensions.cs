﻿using Microsoft.Extensions.DependencyInjection;
using StrawberryShake;
using TezosDomains.NotificationService.Services.Configuration;
using TezosDomains.NotificationService.Services.GraphQL.Generated;

namespace TezosDomains.NotificationService.Services.GraphQL;

public static class GraphQLServiceCollectionExtensions
{
    public static IServiceCollection AddTezosDomainsGQL(this IServiceCollection services)
    {
        services.AddHttpClient("TezosDomainsClient")
            .ConfigureHttpClient(
                (p, c) =>
                {
                    var config = p.GetRequiredService<INotificationConfiguration>();
                    c.BaseAddress = config.TezosDomainsApiUrl;
                    c.DefaultRequestHeaders.Add("X-ClientId", config.TezosDomainsApiClientId);
                });

        services.AddTezosDomainsClient()
            .AddValueSerializer(() => new AddressValueSerializer())
            .AddValueSerializer(() => new MutezValueSerializer());

        return services;
    }
}
