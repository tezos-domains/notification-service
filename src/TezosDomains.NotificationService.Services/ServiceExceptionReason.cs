﻿namespace TezosDomains.NotificationService.Services;

public enum ServiceExceptionReason
{
    KeyNotFound,
    KeyExpired
}
