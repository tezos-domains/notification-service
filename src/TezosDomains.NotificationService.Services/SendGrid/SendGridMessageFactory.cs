﻿using SendGrid.Helpers.Mail;
using System.ComponentModel;
using TezosDomains.NotificationService.Services.Configuration;
using TezosDomains.NotificationService.Services.Model;

namespace TezosDomains.NotificationService.Services.SendGrid;

public class SendGridMessageFactory
{
    private readonly IEmailConfiguration _emailConfiguration;

    public SendGridMessageFactory(IEmailConfiguration emailConfiguration)
    {
        _emailConfiguration = emailConfiguration;
    }

    public SendGridMessage Create(Subscription subscription, string template, object data)
    {
        if (!_emailConfiguration.Templates.ContainsKey(template))
        {
            throw new Exception($"Template '{template}' is not configured.");
        }

        var to = new EmailAddress(subscription.Email);

        var templateData = new Dictionary<string, object>
        {
            ["baseUrl"] = _emailConfiguration.AppUrl,
            ["unsubscribeKey"] = subscription.UnsubscribeKey,
            ["tezosAddress"] = subscription.Address
        };

        foreach (PropertyDescriptor property in TypeDescriptor.GetProperties(data))
        {
            templateData[property.Name] = property.GetValue(data);
        }

        var message = MailHelper.CreateSingleTemplateEmail(
            _emailConfiguration.Sender,
            to,
            _emailConfiguration.Templates[template],
            templateData
        );

        if (!string.IsNullOrEmpty(_emailConfiguration.BccLogAddress))
        {
            message.AddBcc(new EmailAddress(_emailConfiguration.BccLogAddress));
        }

        return message;
    }
}
