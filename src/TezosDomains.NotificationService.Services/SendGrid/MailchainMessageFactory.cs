﻿using System.ComponentModel;
using TezosDomains.NotificationService.Services.Configuration;

namespace TezosDomains.NotificationService.Services.SendGrid;


public class MailchainMessage
{
    public string To { get; init; }
    public string TemplateId { get; init; }
    public object Data { get; init; }

}

public class MailchainMessageFactory
{
    private readonly IEmailConfiguration _emailConfiguration;

    public MailchainMessageFactory(IEmailConfiguration emailConfiguration)
    {
        _emailConfiguration = emailConfiguration;
    }

    public MailchainMessage Create(string to, string template, object data)
    {
        if (!_emailConfiguration.Templates.ContainsKey(template))
        {
            throw new Exception($"Template '{template}' is not configured.");
        }

        var templateData = new Dictionary<string, object>
        {
            ["baseUrl"] = _emailConfiguration.AppUrl,
            ["tezosAddress"] = to,
            ["mailchain"] = true
        };

        foreach (PropertyDescriptor property in TypeDescriptor.GetProperties(data))
        {
            templateData[property.Name] = property.GetValue(data);
        }

        var message = new MailchainMessage
        {
            To = to,
            TemplateId = _emailConfiguration.Templates[template],
            Data = templateData

        };

        return message;
    }
}
