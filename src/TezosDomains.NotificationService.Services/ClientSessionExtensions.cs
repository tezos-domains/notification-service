﻿using MongoDB.Driver;

namespace TezosDomains.NotificationService.Services;

public static class ClientSessionExtensions
{
    public static Task WithTransactionAsync(
        this IClientSession session,
        Func<IClientSessionHandle, CancellationToken, Task> callbackAsync,
        TransactionOptions transactionOptions = null,
        CancellationToken cancellationToken = default
    ) => session.WithTransactionAsync<object>(
        async (h, ct) =>
        {
            await callbackAsync(h, ct);
            return null;
        },
        transactionOptions,
        cancellationToken
    );
}
