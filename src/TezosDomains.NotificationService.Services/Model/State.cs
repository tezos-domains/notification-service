﻿namespace TezosDomains.NotificationService.Services.Model;

public class State
{
    public Guid Id { get; set; }
    public DateTimeOffset LastProcessedEventTimestamp { get; set; }
    public DateTimeOffset? LastProcessedExpirationTimestamp { get; set; }
}
