﻿namespace TezosDomains.NotificationService.Services.Model;

public class Subscription
{
    public Guid Id { get; set; }
    public string Email { get; set; }
    public string Address { get; set; }
    public Guid UnsubscribeKey { get; set; }
    public DateTime CreatedAt { get; set; }
    public DateTime? ConfirmedAt { get; set; }
    public DateTime? UnsubscribedAt { get; set; }
    public DateTime? ReplacedAt { get; set; }
}
