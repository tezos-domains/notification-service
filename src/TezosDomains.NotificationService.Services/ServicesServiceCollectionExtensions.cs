using Microsoft.Extensions.DependencyInjection;
using TezosDomains.NotificationService.Services.GraphQL;
using TezosDomains.NotificationService.Services.Notifications;
using TezosDomains.NotificationService.Services.SendGrid;

namespace TezosDomains.NotificationService.Services;

public static class ServicesServiceCollectionExtensions
{
    public static void AddServices(this IServiceCollection services)
    {
        services
            .AddTransient<MongoDbContext>()
            .AddTransient<MongoDbInitializer>()
            .AddTransient<SubscriptionService>()
            .AddTransient<SendGridMessageFactory>()
            .AddTransient<MailchainMessageFactory>()
            .AddTransient<IClock, Clock>()
            .AddTezosDomainsGQL()
            .AddNotifications();
    }
}
