﻿namespace TezosDomains.NotificationService.Services;

public class SubscriptionInfo
{
    public SubscriptionInfo(string email, string address)
    {
        Email = email;
        Address = address;
    }

    public string Email { get; set; }
    public string Address { get; set; }
}
