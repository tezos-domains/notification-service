﻿namespace TezosDomains.NotificationService.Services;

public enum GenericEmailTemplate
{
    Confirmation,
    AlreadySubscribed
}
