﻿using Microsoft.Extensions.Logging;
using MongoDB.Driver;
using SendGrid.Helpers.Mail;
using TezosDomains.NotificationService.Services.Configuration;
using TezosDomains.NotificationService.Services.Model;
using TezosDomains.NotificationService.Services.SendGrid;

namespace TezosDomains.NotificationService.Services;

public class SubscriptionService
{
    private readonly MongoDbContext _mongoDbContext;
    private readonly ISubscriptionConfiguration _subscriptionConfiguration;
    private readonly SendGridMessageFactory _sendGridMessageFactory;
    private readonly ILogger _logger;

    public SubscriptionService(
        MongoDbContext mongoDbContext,
        ISubscriptionConfiguration subscriptionConfiguration,
        SendGridMessageFactory sendGridMessageFactory,
        ILogger<SubscriptionService> logger
    )
    {
        _mongoDbContext = mongoDbContext;
        _subscriptionConfiguration = subscriptionConfiguration;
        _sendGridMessageFactory = sendGridMessageFactory;
        _logger = logger;
    }

    private static SubscriptionInfo CreateSubscriptionInfo(Subscription subscription)
    {
        return new SubscriptionInfo(subscription.Email, subscription.Address);
    }

    public async Task<SendGridMessage> Subscribe(
        string email,
        string address,
        CancellationToken cancellationToken = default
    )
    {
        var existing = await _mongoDbContext.Subscriptions.FindAsync(
            Builders<Subscription>.Filter.And(
                Builders<Subscription>.Filter.Eq(s => s.Address, address),
                Builders<Subscription>.Filter.Eq(s => s.Email, email),
                Builders<Subscription>.Filter.Eq(s => s.UnsubscribedAt, null),
                Builders<Subscription>.Filter.Ne(s => s.ConfirmedAt, null),
                Builders<Subscription>.Filter.Eq(s => s.ReplacedAt, null)
            ),
            cancellationToken: cancellationToken
        );

        var existingSubscription = await existing.SingleOrDefaultAsync(cancellationToken);

        if (existingSubscription != null)
        {
            _logger.LogDebug("Subscription for Tezos address {address} with e-mail {email} already exists", address, email);
            
            return _sendGridMessageFactory.Create(existingSubscription, GenericEmailTemplate.AlreadySubscribed.ToString(), new { });
        }
        
        // save the subscription
        var now = DateTime.UtcNow;
        var subscription = new Subscription
        {
            Id = Guid.NewGuid(),
            UnsubscribeKey = Guid.NewGuid(),
            Address = address,
            Email = email,
            CreatedAt = now
        };
        await _mongoDbContext.Subscriptions.InsertOneAsync(subscription, cancellationToken: cancellationToken);

        _logger.LogDebug("Subscribed Tezos address {address} with e-mail {email}", address, email);

        // create the confirmation message
        return _sendGridMessageFactory.Create(subscription, GenericEmailTemplate.Confirmation.ToString(), new { confirmKey = subscription.Id.ToString() });
    }

    public async Task<SubscriptionInfo> Confirm(Guid confirmKey, CancellationToken cancellationToken = default)
    {
        var now = DateTime.UtcNow;
        using var session = await _mongoDbContext.Client.StartSessionAsync(cancellationToken: cancellationToken);
        return await session.WithTransactionAsync(
            async (innerSession, ct) =>
            {
                var unconfirmedFilter = Builders<Subscription>.Filter.And(
                    Builders<Subscription>.Filter.Eq(s => s.Id, confirmKey),
                    Builders<Subscription>.Filter.Eq(s => s.UnsubscribedAt, null),
                    Builders<Subscription>.Filter.Eq(s => s.ReplacedAt, null)
                );
                var subscriptions = await _mongoDbContext.Subscriptions.FindAsync(
                    innerSession,
                    unconfirmedFilter,
                    cancellationToken: ct
                );
                var subscription = await subscriptions.SingleOrDefaultAsync(ct)
                                   ?? throw new ServiceException(ServiceExceptionReason.KeyNotFound, $"Invalid confirmKey {confirmKey}");

                // check expiration
                if (now > subscription.ConfirmedAt + _subscriptionConfiguration.ConfirmKeyExpiration)
                {
                    throw new ServiceException(ServiceExceptionReason.KeyExpired, $"confirmKey {confirmKey} has expired");
                }

                // mark confirmed
                await _mongoDbContext.Subscriptions.UpdateOneAsync(
                    Builders<Subscription>.Filter.Eq(s => s.Id, subscription.Id),
                    Builders<Subscription>.Update.Set(s => s.ConfirmedAt, now),
                    cancellationToken: ct
                );

                _logger.LogDebug("Confirmed subscription {id}", subscription.Id);

                // replace any previous confirmed subscription with the same e-mail and address
                var replacedFilter = Builders<Subscription>.Filter.And(
                    Builders<Subscription>.Filter.Eq(s => s.Email, subscription.Email),
                    Builders<Subscription>.Filter.Eq(s => s.Address, subscription.Address),
                    Builders<Subscription>.Filter.Ne(s => s.ConfirmedAt, null),
                    Builders<Subscription>.Filter.Eq(s => s.UnsubscribedAt, null),
                    Builders<Subscription>.Filter.Eq(s => s.ReplacedAt, null),
                    Builders<Subscription>.Filter.Ne(s => s.Id, subscription.Id)
                );
                var replaced = await _mongoDbContext.Subscriptions.FindOneAndUpdateAsync(
                    innerSession,
                    replacedFilter,
                    Builders<Subscription>.Update.Set(s => s.ReplacedAt, now),
                    cancellationToken: ct
                );
                if (replaced != null)
                {
                    _logger.LogDebug("Replaced subscription {replacedId} with {newId}", replaced.Id, subscription.Id);
                }

                return CreateSubscriptionInfo(subscription);
            },
            cancellationToken: cancellationToken
        );
    }

    public async Task<SubscriptionInfo> Unsubscribe(Guid unsubscribeKey, CancellationToken cancellationToken = default)
    {
        var now = DateTime.UtcNow;
        var subscription = await _mongoDbContext.Subscriptions.FindOneAndUpdateAsync(
            Builders<Subscription>.Filter.Eq(s => s.UnsubscribeKey, unsubscribeKey),
            Builders<Subscription>.Update.Set(s => s.UnsubscribedAt, now),
            cancellationToken: cancellationToken
        );
        if (subscription == null)
        {
            throw new ServiceException(ServiceExceptionReason.KeyNotFound, $"Invalid unsubscribeKey {unsubscribeKey}");
        }

        _logger.LogDebug("Unsubscribed {id}", unsubscribeKey);

        return CreateSubscriptionInfo(subscription);
    }
}
