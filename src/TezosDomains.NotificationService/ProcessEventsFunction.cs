using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.Logging;
using TezosDomains.NotificationService.Services.Notifications;

namespace TezosDomains.NotificationService;

public class ProcessEventsFunction
{
    private readonly EventsProcessorService _eventsProcessorService;

    public ProcessEventsFunction(EventsProcessorService eventsProcessorService)
    {
        _eventsProcessorService = eventsProcessorService;
    }

    [FunctionName("ProcessEvents")]
    public async Task Run(
        [TimerTrigger("0 * * * * *", RunOnStartup = true)]
        TimerInfo timerInfo,
        [ServiceBus("notifications")] IAsyncCollector<Notification> serviceBus,
        ILogger log,
        CancellationToken cancellationToken
    )
    {
        var notifications = await _eventsProcessorService.Process(cancellationToken);
        foreach (var notification in notifications)
        {
            await serviceBus.AddAsync(notification, cancellationToken);
        }
    }
}
