﻿using Microsoft.Extensions.DependencyInjection;

namespace TezosDomains.NotificationService;

public static class ServiceCollectionExtensions
{
    public static IServiceCollection ValidateConfiguration(this IServiceCollection services)
    {
        services.AddSingleton<EmailConfigurationValidator>();

        using (var scope = services.BuildServiceProvider().CreateScope())
        {
            var validator = scope.ServiceProvider.GetRequiredService<EmailConfigurationValidator>();
            validator.Validate();

        }

        return services;
    }
}
