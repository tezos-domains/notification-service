using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using TezosDomains.NotificationService.Services;

namespace TezosDomains.NotificationService;

public class InitializeDbFunction
{
    private readonly MongoDbInitializer _initializer;

    public InitializeDbFunction(MongoDbInitializer initializer)
    {
        _initializer = initializer;
    }

    [FunctionName("InitializeDb")]
    public async Task<IActionResult> Run(
        [HttpTrigger(AuthorizationLevel.Admin, "post")]
        HttpRequest request
    )
    {
        await _initializer.Initialize(cancellationToken: request.HttpContext.RequestAborted);
        return new OkResult();
    }
}
