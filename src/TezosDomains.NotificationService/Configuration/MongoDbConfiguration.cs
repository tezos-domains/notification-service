﻿using System.ComponentModel.DataAnnotations;
using TezosDomains.NotificationService.Services.Configuration;

namespace TezosDomains.NotificationService.Configuration;

public class MongoDbConfiguration : IMongoDbConfiguration
{
    [Required] public string ConnectionString { get; set; }
}
