﻿using System.ComponentModel.DataAnnotations;
using TezosDomains.NotificationService.Services.Configuration;

namespace TezosDomains.NotificationService.Configuration;

public class NotificationConfiguration : INotificationConfiguration
{
    [Required] public Uri TezosDomainsApiUrl { get; set; }
    [Required] public string TezosDomainsApiClientId { get; set; }
    [Required] public TimeSpan SkipBlocksOlderThan { get; set; }
    [Required] public IReadOnlyDictionary<string, int> ExpiringSoonThresholds { get; set; }
    public string SendAllExpirationNotificationsTo { get; set; }
}
