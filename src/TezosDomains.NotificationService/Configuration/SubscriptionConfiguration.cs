﻿using System.ComponentModel.DataAnnotations;
using TezosDomains.NotificationService.Services.Configuration;

namespace TezosDomains.NotificationService.Configuration;

public class SubscriptionConfiguration : ISubscriptionConfiguration
{
    [Required] public TimeSpan? ConfirmKeyExpiration { get; set; }
}
