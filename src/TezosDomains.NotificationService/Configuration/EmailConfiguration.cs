﻿using System.ComponentModel.DataAnnotations;
using SendGrid.Helpers.Mail;
using TezosDomains.NotificationService.Services;
using TezosDomains.NotificationService.Services.Configuration;
using TezosDomains.NotificationService.Services.Notifications;

namespace TezosDomains.NotificationService.Configuration;

public class EmailConfiguration : IEmailConfiguration, IValidatableObject
{
    [Required]
    [DataType(DataType.EmailAddress)]
    public string SenderAddress { get; set; }

    [Required] public string SenderName { get; set; }

    public string BccLogAddress { get; set; }

    [Required] public IReadOnlyDictionary<string, string> Templates { get; set; }
    [Required] public Uri AppUrl { get; set; }
    public EmailAddress Sender => new(SenderAddress, SenderName);

    public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
    {
        var ignoredTemplates = new HashSet<string> { NotificationType.ExpiringSoon.ToString() };
        var requiredTemplates = Enum.GetNames(typeof(NotificationType)).Concat(Enum.GetNames(typeof(GenericEmailTemplate))).Where(x => !ignoredTemplates.Contains(x));

        var missingTemplates = requiredTemplates.Except(Templates.Keys).ToList();

        if (missingTemplates.Any())
        {
            yield return new ValidationResult(
                $"Following email templates are not configured: {string.Join(", ", missingTemplates)}",
                new[] { nameof(Templates) }
            );
        }
    }
}
