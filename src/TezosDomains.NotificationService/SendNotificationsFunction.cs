using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.Logging;
using SendGrid.Helpers.Mail;
using TezosDomains.NotificationService.Services.Notifications;

namespace TezosDomains.NotificationService;

public class SendNotificationsFunction
{
    private readonly NotificationEmailService _notificationEmailService;

    public SendNotificationsFunction(NotificationEmailService notificationEmailService)
    {
        _notificationEmailService = notificationEmailService;
    }

    [FunctionName("SendNotifications")]
    public async Task Run(
        [ServiceBusTrigger("notifications")] Notification notification,
        [SendGrid] IAsyncCollector<SendGridMessage> messageCollector,
        ILogger log,
        CancellationToken cancellationToken
    )
    {
        var messages = await _notificationEmailService.GenerateEmails(notification, cancellationToken);
        foreach (var message in messages)
        {
            await messageCollector.AddAsync(message, cancellationToken);
        }
    }
}
