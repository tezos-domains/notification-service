﻿using Microsoft.Azure.Functions.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using TezosDomains.NotificationService;
using TezosDomains.NotificationService.Configuration;
using TezosDomains.NotificationService.Services;
using TezosDomains.NotificationService.Services.Configuration;

[assembly: FunctionsStartup(typeof(Startup))]

namespace TezosDomains.NotificationService;

public class Startup : FunctionsStartup
{
    public override void ConfigureAppConfiguration(IFunctionsConfigurationBuilder builder)
    {
        var context = builder.GetContext();

        builder.ConfigurationBuilder
            .AddJsonFile(
                Path.Combine(context.ApplicationRootPath, "local.settings.json"),
                optional: true,
                reloadOnChange: false
            )
            .AddUserSecrets<Startup>(optional: true)
            .AddEnvironmentVariables();
    }

    public override void Configure(IFunctionsHostBuilder builder)
    {
        var configuration = builder.GetContext().Configuration;
        builder.Services
            .AddSingleton<IMongoDbConfiguration>(configuration.GetValidated<MongoDbConfiguration>("MongoDb"))
            .AddSingleton<IEmailConfiguration>(configuration.GetValidated<EmailConfiguration>("Email"))
            .AddSingleton<ISubscriptionConfiguration>(configuration.GetValidated<SubscriptionConfiguration>("Subscription"))
            .AddSingleton<INotificationConfiguration>(configuration.GetValidated<NotificationConfiguration>("Notification"));

        builder.Services.ValidateConfiguration();
        builder.Services.AddServices();
    }
}
