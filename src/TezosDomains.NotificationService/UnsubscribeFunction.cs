using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using TezosDomains.NotificationService.Services;

namespace TezosDomains.NotificationService;

public class UnsubscribeFunction
{
    public class UnsubscribeRequest
    {
        public string Key { get; set; }
    }

    private readonly SubscriptionService _service;

    public UnsubscribeFunction(SubscriptionService service)
    {
        _service = service;
    }

    [FunctionName("Unsubscribe")]
    public async Task<IActionResult> Run(
        [HttpTrigger(AuthorizationLevel.Anonymous, "post")]
        UnsubscribeRequest request,
        HttpRequest httpRequest
    )
    {
        try
        {
            var key = Guid.Parse(request.Key);
            var info = await _service.Unsubscribe(key, cancellationToken: httpRequest.HttpContext.RequestAborted);

            return new OkObjectResult(info);
        }
        catch (ServiceException e)
        {
            return e.ToActionResult();
        }
    }
}
