﻿using TezosDomains.NotificationService.Services.Configuration;
using TezosDomains.NotificationService.Services.Notifications;

namespace TezosDomains.NotificationService;

public class EmailConfigurationValidator
{
    private readonly IEmailConfiguration _emailConfiguration;
    private readonly INotificationConfiguration _notificationConfiguration;

    public EmailConfigurationValidator(IEmailConfiguration emailConfiguration, INotificationConfiguration notificationConfiguration)
    {
        _emailConfiguration = emailConfiguration;
        _notificationConfiguration = notificationConfiguration;
    }

    public void Validate()
    {
        foreach (var threshold in _notificationConfiguration.ExpiringSoonThresholds)
        {
            var templateKey = $"{NotificationType.ExpiringSoon}-{threshold.Key}";
            if (!_emailConfiguration.Templates.ContainsKey(templateKey))
            {
                throw new InvalidOperationException($"Missing required template in the email configuration: `{templateKey}`");
            }

        }
    }
}
