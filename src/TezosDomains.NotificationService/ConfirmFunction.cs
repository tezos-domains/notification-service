using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using TezosDomains.NotificationService.Services;

namespace TezosDomains.NotificationService;

public class ConfirmFunction
{
    public class ConfirmRequest
    {
        public string Key { get; set; }
    }

    private readonly SubscriptionService _service;

    public ConfirmFunction(SubscriptionService service)
    {
        _service = service;
    }

    [FunctionName("Confirm")]
    public async Task<IActionResult> Run(
        [HttpTrigger(AuthorizationLevel.Anonymous, "post")]
        ConfirmRequest request,
        HttpRequest httpRequest
    )
    {
        var key = Guid.Parse(request.Key);
        try
        {
            var info = await _service.Confirm(key, cancellationToken: httpRequest.HttpContext.RequestAborted);
            return new OkObjectResult(info);
        }
        catch (ServiceException e)
        {
            return e.ToActionResult();
        }
    }
}
