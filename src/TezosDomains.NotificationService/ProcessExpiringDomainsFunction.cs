using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.Logging;
using TezosDomains.NotificationService.Services.Notifications;
using TezosDomains.NotificationService.Services.SendGrid;

namespace TezosDomains.NotificationService;

public class ProcessExpiringDomainsFunction
{
    private readonly ExpiringDomainsProcessorService _expiringDomainsProcessorService;
    private readonly NotificationEmailService _notificationEmailService;

    public ProcessExpiringDomainsFunction(ExpiringDomainsProcessorService expiringDomainsProcessorService, NotificationEmailService notificationEmailService)
    {
        _expiringDomainsProcessorService = expiringDomainsProcessorService;
        _notificationEmailService = notificationEmailService;
    }

    [FunctionName("ProcessExpiringDomains")]
    public async Task Run(
        [TimerTrigger("%ExpiringDomainsFunctionSchedule%", RunOnStartup = true)]
        TimerInfo timerInfo,
        [ServiceBus("notifications")] IAsyncCollector<Notification> serviceBus,
        [ServiceBus("mailchain-notifications")] IAsyncCollector<MailchainMessage> serviceBusMailchain,
        ILogger log,
        CancellationToken cancellationToken
    )
    {
        var notifications = await _expiringDomainsProcessorService.Process(cancellationToken);
        foreach (var notification in notifications)
        {
            await serviceBus.AddAsync(notification, cancellationToken);
            await serviceBusMailchain.AddAsync(_notificationEmailService.GenerateMailchain(notification), cancellationToken);
        }
    }
}
