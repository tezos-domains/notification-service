﻿using Microsoft.AspNetCore.Mvc;
using TezosDomains.NotificationService.Services;

namespace TezosDomains.NotificationService;

public static class ServiceExceptionExtensions
{
    public static IActionResult ToActionResult(this ServiceException exception) => new BadRequestObjectResult(new { reason = exception.Reason.ToString() });
}
