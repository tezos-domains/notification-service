﻿using System.ComponentModel.DataAnnotations;
using Microsoft.Extensions.Configuration;

namespace TezosDomains.NotificationService;

public static class ConfigurationExtensions
{
    public static T GetValidated<T>(this IConfiguration configuration, string name)
    {
        var instance = configuration.GetSection(name).Get<T>()
                       ?? throw new InvalidOperationException($"Missing required configuration section {name} of type {typeof(T).Name}");
        Validator.ValidateObject(instance, new ValidationContext(instance), validateAllProperties: true);
        return instance;
    }
}
