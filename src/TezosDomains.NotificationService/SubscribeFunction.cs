using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using SendGrid.Helpers.Mail;
using TezosDomains.NotificationService.Services;

namespace TezosDomains.NotificationService;

public class SubscribeFunction
{
    public class SubscribeRequest
    {
        public string Address { get; set; }
        public string Email { get; set; }
    }

    private readonly SubscriptionService _service;

    public SubscribeFunction(SubscriptionService service)
    {
        _service = service;
    }

    [FunctionName("Subscribe")]
    public async Task<IActionResult> Run(
        [HttpTrigger(AuthorizationLevel.Anonymous, "post")]
        SubscribeRequest request,
        HttpRequest httpRequest,
        [SendGrid] IAsyncCollector<SendGridMessage> messageCollector
    )
    {
        var message =
            await _service.Subscribe(request.Email, request.Address, cancellationToken: httpRequest.HttpContext.RequestAborted);
        await messageCollector.AddAsync(message);

        return new OkResult();
    }
}
