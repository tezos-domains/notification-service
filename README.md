# Tezos Domains Notification Service

## Deploying to Azure

### Requirements
* [.NET Core SDK 3.1](https://dotnet.microsoft.com/download)
* [Azure CLI](https://docs.microsoft.com/en-us/cli/azure/install-azure-cli)
* [Azure Functions Core Tools](https://docs.microsoft.com/en-us/azure/azure-functions/functions-run-local)

### (Optional) Create a service principal
1. Log in to Azure
```
az login
```
2. Create a new service principal for deployments
```
az ad sp create-for-rbac --name <your function app name>
```

### Deploy
1. Login to your service principal (if you've created one)
```
az login --service-principal -u <your principal ID> -p <your principal password> --tenant <your tenant>
```
2. Deploy the function app
```
func azure functionapp publish <your function app name> --csharp
```
