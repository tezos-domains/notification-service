﻿using FluentAssertions;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using NSubstitute;
using NUnit.Framework;
using SendGrid.Helpers.Mail;
using TezosDomains.NotificationService.Services.Configuration;
using TezosDomains.NotificationService.Services.IntegrationTests.Helpers;
using TezosDomains.NotificationService.Services.Model;
using TezosDomains.NotificationService.Services.Notifications;
using TezosDomains.NotificationService.Services.SendGrid;

namespace TezosDomains.NotificationService.Services.IntegrationTests;

public class NotificationEmailServiceTest : TestBase
{
    private readonly string Address1 = "tz11";
    private readonly string Address2 = "tz12";
    private readonly string Address3 = "tz13";
    private readonly string Address4 = "tz14";

    private readonly string Email11 = "test11@test.com";
    private readonly string Email12 = "test12@test.com";
    private readonly string EmailNo = "no@test.com";

    private NotificationEmailService _service;
    private IEmailConfiguration _emailConfiguration;
    private CancellationToken _ct;

    private async Task AddSubscription(string address, string email, bool isConfirmed, bool isUnsubscribed, bool isReplaced)
    {
        await DbContext.Subscriptions.InsertOneAsync(
            new Subscription
            {
                Id = Guid.NewGuid(),
                UnsubscribeKey = Guid.NewGuid(),
                Email = email,
                Address = address,
                ConfirmedAt = isConfirmed ? DateTime.Now.Subtract(TimeSpan.FromDays(1)) : (DateTime?)null,
                UnsubscribedAt = isUnsubscribed ? DateTime.Now.Subtract(TimeSpan.FromDays(1)) : (DateTime?)null,
                ReplacedAt = isReplaced ? DateTime.Now.Subtract(TimeSpan.FromDays(1)) : (DateTime?)null
            },
            cancellationToken: default
        );
    }

    [SetUp]
    public void Setup()
    {
        _emailConfiguration = Substitute.For<IEmailConfiguration>();

        _emailConfiguration.BccLogAddress.Returns("bcc@my.domain");
        _emailConfiguration.Sender.Returns(new EmailAddress("sender@tezos.domains", "TD"));
        _emailConfiguration.Templates.Returns(
            new Dictionary<string, string>
            {
                { "Outbid", "template" },
                { "Outbid-some-subtype", "some-other-template" },
            }
        );

        _ct = TestCancellationToken.Get();

        _service = new NotificationEmailService(
            DbContext,
            new SendGridMessageFactory(_emailConfiguration),
            new Logger<NotificationEmailService>(new NullLoggerFactory()),
            new MailchainMessageFactory(_emailConfiguration)
            );
    }

    [Test]
    public async Task GenerateEmails_ShouldGenerateAnEmailForEachSubscription()
    {
        await AddSubscription(Address1, Email11, true, false, false);
        await AddSubscription(Address1, Email12, true, false, false);

        var notification = new Notification { RecipientTezosAddress = Address1, Type = NotificationType.Outbid };

        var messages = await _service.GenerateEmails(notification, _ct);

        AssertEmails(messages, Email11, Email12);
    }

    [Test]
    public async Task GenerateEmails_ShouldGenerateAnEmailWithCorrectTemplateName()
    {
        await AddSubscription(Address1, Email11, true, false, false);

        var notification = new Notification { RecipientTezosAddress = Address1, Type = NotificationType.Outbid, SubType = "some-subtype" };

        var messages = await _service.GenerateEmails(notification, _ct);

        messages[0].TemplateId.Should().Be("some-other-template");
    }

    [Test]
    public async Task GenerateEmails_ShouldNotGenerateEmailIfTheresNoSubscription()
    {
        var notification = new Notification { RecipientTezosAddress = "tz1404", Type = NotificationType.Outbid };

        var messages = await _service.GenerateEmails(notification, _ct);

        AssertEmails(messages);
    }

    [Test]
    public async Task GenerateEmails_ShouldNotGenerateEmailForUnsubscribedSubscription()
    {
        await AddSubscription(Address2, EmailNo, true, true, false);

        var notification = new Notification { RecipientTezosAddress = Address1, Type = NotificationType.Outbid };

        var messages = await _service.GenerateEmails(notification, _ct);

        AssertEmails(messages);
    }

    [Test]
    public async Task GenerateEmails_ShouldNotAddBCCIfAddressEmpty()
    {
        _emailConfiguration.BccLogAddress.Returns(string.Empty);

        await AddSubscription(Address1, Email11, true, false, false);

        var notification = new Notification { RecipientTezosAddress = Address1, Type = NotificationType.Outbid };

        var messages = await _service.GenerateEmails(notification, _ct);

        messages[0].Personalizations[0].Bccs.Should().BeNull();
    }

    [Test]
    public async Task GenerateEmails_ShouldNotGenerateEmailForUnconfirmedSubscription()
    {
        await AddSubscription(Address3, EmailNo, false, false, false);

        var notification = new Notification { RecipientTezosAddress = Address1, Type = NotificationType.Outbid };

        var messages = await _service.GenerateEmails(notification, _ct);

        AssertEmails(messages);
    }

    [Test]
    public async Task GenerateEmails_ShouldNotGenerateEmailForReplacedSubscription()
    {
        await AddSubscription(Address4, EmailNo, false, false, true);

        var notification = new Notification { RecipientTezosAddress = Address1, Type = NotificationType.Outbid };

        var messages = await _service.GenerateEmails(notification, _ct);

        AssertEmails(messages);
    }

    private void AssertEmails(IReadOnlyList<SendGridMessage> messages, params string[] emails)
    {
        messages.Should().HaveCount(emails.Length);

        for (var i = 0; i < messages.Count; i++)
        {
            var message = messages[i];
            message.TemplateId.Should().Be("template");
            message.Personalizations[0].Tos[0].Email.Should().Be(emails[i]);
            message.Personalizations[0].Bccs[0].Email.Should().Be(_emailConfiguration.BccLogAddress);
        }
    }
}
