﻿using FluentAssertions;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using NSubstitute;
using NUnit.Framework;
using StrawberryShake;
using TezosDomains.NotificationService.Services.Configuration;
using TezosDomains.NotificationService.Services.GraphQL.Generated;
using TezosDomains.NotificationService.Services.IntegrationTests.Helpers;
using TezosDomains.NotificationService.Services.Notifications;
using TezosDomains.NotificationService.Services.Helpers;

namespace TezosDomains.NotificationService.Services.IntegrationTests;

public class ExpiringDomainsProcessorServiceTest : TestBase
{
#pragma warning disable IDE1006 // Naming Styles

    private readonly DateTimeOffset LastRun = new DateTimeOffset(2020, 12, 31, 0, 0, 0, TimeSpan.Zero);
    private readonly DateTimeOffset Now = new DateTimeOffset(2021, 1, 1, 1, 0, 0, TimeSpan.Zero);
    private readonly DateTimeOffset Date1 = new DateTimeOffset(2021, 1, 2, 10, 5, 0, TimeSpan.Zero);
    private readonly DateTimeOffset Date2 = new DateTimeOffset(2021, 1, 2, 10, 6, 0, TimeSpan.Zero);
    private readonly DateTimeOffset Date3 = new DateTimeOffset(2021, 1, 2, 10, 7, 0, TimeSpan.Zero);

#pragma warning restore IDE1006 // Naming Styles

    private ExpiringDomainsProcessorService _service;
    private ITezosDomainsClient _tezosDomainsClientMock;
    private IClock _clockMock;
    private CancellationToken _ct;
    private List<IOperationResult<IDomains>> _responses;
    private StateService _stateService;
    private INotificationConfiguration _notificationConfig;

    [SetUp]
    public void SetUp()
    {
        _ct = TestCancellationToken.Get();
        _tezosDomainsClientMock = Substitute.For<ITezosDomainsClient>();
        _clockMock = Substitute.For<IClock>();
        _stateService = new StateService(DbContext);

        _notificationConfig = Substitute.For<INotificationConfiguration>();
        _notificationConfig.TezosDomainsApiUrl.Returns(new Uri("http://api.com"));
        _notificationConfig.TezosDomainsApiClientId.Returns(Guid.NewGuid().ToString());
        _notificationConfig.ExpiringSoonThresholds.Returns(new Dictionary<string, int> { { "first-threshold", 30 } });

        _service = new ExpiringDomainsProcessorService(
            _tezosDomainsClientMock,
            _stateService,
            _notificationConfig,
            _clockMock,
            new Logger<ExpiringDomainsProcessorService>(new NullLoggerFactory())
        );

        _clockMock.UtcNow.Returns(Now);


        _tezosDomainsClientMock.DomainsAsync(cancellationToken: default)
            .ReturnsForAnyArgs(
                c =>
                {
                    var after = c.ArgAt<Optional<string>>(1);
                    var index = after != null && after.HasValue ? int.Parse(after) : 0;
                    var response = _responses[index];

                    return Task.FromResult(response);
                }
            );
    }

    [Test]
    public async Task Process_ShouldGetAllPagesOfExpiringDomainsFromApiAndGenerateNotifications()
    {
        _responses = BuildResponse(new[] { ("test1.tez", Date2, "tz1Own1"), ("test2.tez", Date1, "tz1Own2") });

        var notifications = await _service.Process(_ct);

        AssertDefaultNotifications(notifications, "first-threshold");

        await AssertState();
    }

    [Test]
    public async Task Process_ShouldSendNotificationsToAHardcodedAddress()
    {
        _notificationConfig.SendAllExpirationNotificationsTo.Returns("tz1HardCoded2");
        _responses = BuildResponse(new[] { ("test1.tez", Date2, "tz1Own1"), ("test2.tez", Date1, "tz1Own2") });

        var notifications = await _service.Process(_ct);

        AssertDefaultNotifications(notifications, "first-threshold", owner: "tz1HardCoded2");

        await AssertState();
    }

    [Test]
    public async Task Process_ShouldStoreSameProcessedDateForMultipleIntervals()
    {
        _notificationConfig.ExpiringSoonThresholds.Returns(new Dictionary<string, int> { { "first-threshold", 30 }, { "next-threshold", 15 } });
        _responses = BuildResponse(null);

        await _service.Process(_ct);

        await AssertState();
    }

    [Test]
    public async Task Process_ShouldGetAnyExpiringDomainsWithinConfiguredLimit_WhenDBIsEmpty()
    {
        _responses = BuildResponse(new[] { ("test1.tez", Date2, "tz1Own1"), ("test2.tez", Date1, "tz1Own2") });

        await _service.Process(_ct);

        AssertDefaultGQLRequests(DateTimeOffset.MinValue.AddDays(30), Now.AddDays(30).EndOfDay());

        await AssertState();
    }

    [Test]
    public async Task Process_ShouldNotRunIfProcessedAlready()
    {
        _responses = BuildResponse(null);
        await _stateService.SetLastProcessedExpiry(Date3, _ct);

        await _service.Process(_ct);

        _tezosDomainsClientMock.ReceivedCalls().ToList().Should().BeEmpty();
        await AssertState(Date3);
    }


    [Test]
    public async Task Process_ShouldLimitDomainsToOnesThatWerentProcessedAlready()
    {
        _responses = BuildResponse(null);
        await _stateService.SetLastProcessedExpiry(LastRun, _ct);

        await _service.Process(_ct);

        AssertGQLRequest(0, null, LastRun.AddDays(30), Now.AddDays(30).EndOfDay());
        await AssertState();
    }


    [Test]
    public async Task Process_UpdateStateWithTheDateOfTheLastLoadedDomain()
    {
        _responses = BuildResponse(new[] { ("test1.tez", Date2, "tz1Own1"), ("test2.tez", Date1, "tz1Own2") });
        await _stateService.SetLastProcessedExpiry(LastRun, _ct);

        await _service.Process(_ct);

        await AssertState();
    }

    [Test]
    public async Task Process_NotChangeState_WhenEmptyResponse()
    {
        _responses = BuildResponse(null);
        await _stateService.SetLastProcessedExpiry(LastRun, _ct);

        var notifications = await _service.Process(_ct);

        notifications.Should().HaveCount(0);
        await AssertState();
    }

    [Test]
    public void Process_ShouldStopAndReturn_WhenGQLErrorOccurs()
    {
        _responses = BuildResponse(new[] { ("test1.tez", Date2, "tz1Own1"), ("test2.tez", Date1, "tz1Own2") });

        var error = Substitute.For<IOperationResult<IDomains>>();
        error.When(e => e.EnsureNoErrors()).Do(c => throw new Exception("err"));

        _responses[1] = error;

        Func<Task<IReadOnlyCollection<Notification>>> action = async () => await _service.Process(_ct);

        action.Should().ThrowAsync<Exception>().WithMessage("err");
    }
    private List<IOperationResult<IDomains>> BuildResponse((string name, DateTimeOffset expirationDate, string owner)[] data)
    {
        var result = data?.Select((x, index) =>
        {
            var result = Substitute.For<IOperationResult<IDomains>>();
            result.Data.Returns(BuildDomain(x.name, x.expirationDate, x.owner, index + 1, isLastPage: index == data.Length - 1));

            return result;
        }).ToList();

        if (result == null)
        {
            var empty = Substitute.For<IOperationResult<IDomains>>();
            empty.Data.Returns(new Domains1(new DomainConnection(new IDomainEdge[] { }, new PageInfo1(null, false))));
            return new List<IOperationResult<IDomains>> { empty };
        }

        return result;
    }

    private Domains1 BuildDomain(string name, DateTimeOffset expirationDate, string owner, int pageNumber, bool isLastPage)
    {
        return new Domains1(
                new DomainConnection(
                    new[]
                    {
                        new DomainEdge(new Domain(name,expirationDate,owner))
                    },
                    new PageInfo1(pageNumber.ToString(), !isLastPage)
                )
            );
    }

    private void AssertDefaultNotifications(IReadOnlyCollection<Notification> notifications, string expirationType, string owner = null)
    {
        notifications.Should().HaveCount(2);

        AssertNotification(notifications.ElementAt(0), "test1.tez", owner ?? "tz1Own1", 1, Date2, expirationType);
        AssertNotification(notifications.ElementAt(1), "test2.tez", owner ?? "tz1Own2", 1, Date1, expirationType);
    }

    private void AssertNotification(Notification notification, string name, string owner, int daysToExpiration, DateTimeOffset expiry, string expirationType)
    {
        notification.Type.Should().Be(NotificationType.ExpiringSoon);
        notification.RecipientTezosAddress.Should().Be(owner);
        notification.Data.Should().BeEquivalentTo(new { daysToExpiration, owner, domains = new[] { new { name, expires = expiry.ToString("MMMM dd, yyyy") } } });
        notification.SubType.Should().Be(expirationType);
    }

    private async Task AssertState(DateTimeOffset? specificDate = null)
    {
        var state = await _stateService.Get(_ct);
        state.LastProcessedExpirationTimestamp.Should().Be(specificDate ?? Now.EndOfDay());
    }

    private void AssertDefaultGQLRequests(DateTimeOffset from, DateTimeOffset to)
    {
        _tezosDomainsClientMock.ReceivedWithAnyArgs(2).DomainsAsync(cancellationToken: default);

        AssertGQLRequest(0, null, from, to);
        AssertGQLRequest(1, "1", from, to);
    }

    private void AssertGQLRequest(int index, string expectedAfter, DateTimeOffset? expectedFrom, DateTimeOffset expectedTo)
    {
        var calls = _tezosDomainsClientMock.ReceivedCalls().Where(c => c.GetMethodInfo().Name == "DomainsAsync").ToList();

        var call = calls[index];

        var args = call.GetArguments();

        var first = (Optional<int?>)args[0];
        var after = (Optional<string>)args[1];
        var where = (Optional<DomainsFilter>)args[2];
        var order = (Optional<DomainOrder>)args[3];
        var cancellationToken = (CancellationToken)args[4];



        first.Value.Should().Be(50);
        after.Value.Should().Be(expectedAfter);
        where.Value.Should()
            .BeEquivalentTo(
                new
                {
                    ExpiresAtUtc = new
                    {
                        Value = new
                        {
                            LessThanOrEqualTo = new
                            {
                                Value = expectedTo
                            },
                            GreaterThan = new
                            {
                                Value = expectedFrom
                            }
                        }
                    },
                    Level = new
                    {
                        Value = new
                        {
                            EqualTo = new
                            {
                                Value = 2
                            }
                        }
                    }
                }
            );
        order.Value.Should()
            .BeEquivalentTo(
                new DomainOrder
                {
                    Field = DomainOrderField.ExpiresAt,
                    Direction = OrderDirection.Asc
                }
            );
        cancellationToken.Should().Be(_ct);
    }
}
