﻿using FluentAssertions;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using NSubstitute;
using NUnit.Framework;
using StrawberryShake;
using TezosDomains.NotificationService.Services.Configuration;
using TezosDomains.NotificationService.Services.GraphQL.Generated;
using TezosDomains.NotificationService.Services.IntegrationTests.Helpers;
using TezosDomains.NotificationService.Services.Notifications;
using TezosDomains.NotificationService.Services.Notifications.Converters;

namespace TezosDomains.NotificationService.Services.IntegrationTests;

public class EventsProcessorServiceTest : TestBase
{
#pragma warning disable IDE1006 // Naming Styles
    private readonly DateTimeOffset Now = new DateTimeOffset(2021, 1, 1, 1, 0, 0, TimeSpan.Zero);
    private readonly DateTimeOffset Date1 = new DateTimeOffset(2021, 1, 1, 10, 5, 0, TimeSpan.Zero);
    private readonly DateTimeOffset Date2 = new DateTimeOffset(2021, 1, 1, 10, 6, 0, TimeSpan.Zero);
    private readonly DateTimeOffset Date3 = new DateTimeOffset(2021, 1, 1, 10, 7, 0, TimeSpan.Zero);
    private readonly DateTimeOffset Date4 = new DateTimeOffset(2021, 1, 1, 10, 8, 0, TimeSpan.Zero);
    private readonly DateTimeOffset Date5 = new DateTimeOffset(2021, 1, 1, 10, 9, 0, TimeSpan.Zero);

#pragma warning restore IDE1006 // Naming Styles

    private EventsProcessorService _service;
    private ITezosDomainsClient _tezosDomainsClientMock;
    private IClock _clockMock;
    private CancellationToken _ct;
    private List<IOperationResult<IEvents>> _responses;
    private StateService _stateService;

    public class MockNotificationConfiguration : INotificationConfiguration
    {
        public Uri TezosDomainsApiUrl => new Uri("http://api.com");
        public string TezosDomainsApiClientId => Guid.NewGuid().ToString();
        public TimeSpan SkipBlocksOlderThan => TimeSpan.FromDays(1);
        public IReadOnlyDictionary<string, int> ExpiringSoonThresholds => new Dictionary<string, int>();
        public string SendAllExpirationNotificationsTo => null;
    }

    [SetUp]
    public void SetUp()
    {
        _ct = TestCancellationToken.Get();
        _tezosDomainsClientMock = Substitute.For<ITezosDomainsClient>();
        _clockMock = Substitute.For<IClock>();
        _stateService = new StateService(DbContext);

        _service = new EventsProcessorService(
            _tezosDomainsClientMock,
            _stateService,
            new MockNotificationConfiguration(),
            new IEventToNotificationsConverter[] { new AuctionEndEventConverter(), new AuctionBidEventConverter(), new OfferExecutedEventConverter(), new BuyOfferPlacedEventConverter(), new BuyOfferExecutedEventConverter() },
            _clockMock,
            new Logger<EventsProcessorService>(new NullLoggerFactory())
        );

        _clockMock.UtcNow.Returns(Now);

        var outbidResponse = Substitute.For<IOperationResult<IEvents>>();
        outbidResponse.Data.Returns(
            new Events1(
                new EventConnection(
                    new[]
                    {
                        new EventEdge(
                            new AuctionBidEvent(
                                EventType.AuctionBidEvent,
                                "tz1Bid",
                                new Block(1, Date1),
                                "necroskillz.tez",
                                2e6m,
                                "tz1Outbid"
                            )
                        )
                    },
                    new PageInfo("1", true)
                )
            )
        );

        var auctionEndResponse = Substitute.For<IOperationResult<IEvents>>();
        auctionEndResponse.Data.Returns(
            new Events1(
                new EventConnection(
                    new[]
                    {
                        new EventEdge(
                            new AuctionEndEvent(
                                EventType.AuctionEndEvent,
                                "tz1Won",
                                new Block(2, Date2),
                                "alice.tez",
                                new List<IAuctionParticipant>
                                {
                                    new AuctionParticipant("tz1Won"),
                                    new AuctionParticipant("tz1Lost")
                                }
                            )
                        )
                    },
                    new PageInfo("2", true)
                )
            )
        );

        var offerExecutedResponse = Substitute.For<IOperationResult<IEvents>>();
        offerExecutedResponse.Data.Returns(
            new Events1(
                new EventConnection(
                    new[]
                    {
                        new EventEdge(
                            new OfferExecutedEvent(
                                EventType.OfferExecutedEvent,
                                "tz1Buyer",
                                new Block(3, Date3),
                                "alice.tez",
                                3e6m,
                                "tz1Seller"
                            )
                        )
                    },
                    new PageInfo("3", true)
                )
            )
        );

        var buyOfferPlacedResponse = Substitute.For<IOperationResult<IEvents>>();
        buyOfferPlacedResponse.Data.Returns(
            new Events1(
                new EventConnection(
                    new[]
                    {
                        new EventEdge(
                            new BuyOfferPlacedEvent(
                                EventType.BuyOfferPlacedEvent,
                                "tz1Seller",
                                new Block(4, Date4),
                                "alice.tez",
                                "tz1Owner",
                                4e6m
                            )
                        )
                    },
                    new PageInfo("4", true)
                )
            )
        );

        var buyOfferExecutedResponse = Substitute.For<IOperationResult<IEvents>>();
        buyOfferExecutedResponse.Data.Returns(
            new Events1(
                new EventConnection(
                    new[]
                    {
                        new EventEdge(
                            new BuyOfferExecutedEvent(
                                EventType.BuyOfferExecutedEvent,
                                "tz1Seller",
                                new Block(5, Date5),
                                "alice.tez",
                                "tz1Buyer"
                            )
                        )
                    },
                    new PageInfo("5", false)
                )
            )
        );

        _responses = new List<IOperationResult<IEvents>> { outbidResponse, auctionEndResponse, offerExecutedResponse, buyOfferPlacedResponse, buyOfferExecutedResponse };

        _tezosDomainsClientMock.EventsAsync(cancellationToken: default)
            .ReturnsForAnyArgs(
                c =>
                {
                    var after = c.ArgAt<Optional<string>>(1);
                    var index = after != null && after.HasValue ? int.Parse(after) : 0;
                    var response = _responses[index];

                    return Task.FromResult(response);
                }
            );
    }

    [Test]
    public async Task Process_ShouldGetAllPagesOfEventsFromApiAndGenerateNotifications()
    {
        var notifications = await _service.Process(_ct);

        AssertDefaultNotifications(notifications);

        await AssertState(Date5);
    }

    [Test]
    public async Task Process_ShouldStartFromNonOldTimestamp_WhenDBIsEmpty()
    {
        var notifications = await _service.Process(_ct);

        AssertDefaultGQLRequests(timestamp: Now.Subtract(TimeSpan.FromDays(1)));

        AssertDefaultNotifications(notifications);

        await AssertState(Date5);
    }

    [Test]
    public async Task Process_ShouldStartFromNonOldTimestamp_WhenStateIsOld()
    {
        await _stateService.SetLastProcessedEvent(Now.Subtract(TimeSpan.FromDays(2)), _ct);

        var notifications = await _service.Process(_ct);

        AssertDefaultGQLRequests(timestamp: Now.Subtract(TimeSpan.FromDays(1)));

        AssertDefaultNotifications(notifications);

        await AssertState(Date5);
    }

    [Test]
    public async Task Process_ShouldLastProcessedTimestamp()
    {
        await _stateService.SetLastProcessedEvent(Now.Subtract(TimeSpan.FromHours(2)), _ct);

        var notifications = await _service.Process(_ct);

        AssertDefaultGQLRequests(Now.Subtract(TimeSpan.FromHours(2)));

        AssertDefaultNotifications(notifications);

        await AssertState(Date5);
    }

    [Test]
    public async Task Process_NotChangeState_WhenEmptyResponse()
    {
        await _stateService.SetLastProcessedEvent(Now, _ct);

        var empty = Substitute.For<IOperationResult<IEvents>>();

        empty.Data.Returns(new Events1(new EventConnection(new IEventEdge[] { }, new PageInfo(null, false))));

        _responses[0] = empty;

        var notifications = await _service.Process(_ct);

        notifications.Should().HaveCount(0);

        await AssertState(Now);
    }

    [Test]
    public async Task Process_ShouldStopAndReturn_WhenGQLErrorOccurs()
    {
        var error = Substitute.For<IOperationResult<IEvents>>();
        error.When(e => e.EnsureNoErrors()).Do(c => throw new Exception("err"));

        _responses[1] = error;

        var notifications = await _service.Process(_ct);

        notifications.Should().HaveCount(1);

        AssertOutbidNotification(notifications[0]);

        await AssertState(Date1);
    }

    private void AssertDefaultNotifications(List<Notification> notifications)
    {
        notifications.Should().HaveCount(6);

        AssertOutbidNotification(notifications[0]);
        AssertAuctionWonNotification(notifications[1]);
        AssertAuctionLostNotification(notifications[2]);
        AssertOfferSoldNotification(notifications[3]);
        AssertBuyOfferPlacedNotification(notifications[4]);
        AssertBuyOfferExecutedNotification(notifications[5]);

    }

    private void AssertOutbidNotification(Notification notification)
    {
        notification.Type.Should().Be(NotificationType.Outbid);
        notification.BlockLevel.Should().Be(1);
        notification.BlockTimestamp.Should().Be(Date1);
        notification.RecipientTezosAddress.Should().Be("tz1Outbid");
        notification.Data.Should().BeEquivalentTo(new { domainName = "necroskillz.tez", bidAmount = "2" });
    }

    private void AssertAuctionWonNotification(Notification notification)
    {
        notification.Type.Should().Be(NotificationType.AuctionWon);
        notification.BlockLevel.Should().Be(2);
        notification.BlockTimestamp.Should().Be(Date2);
        notification.RecipientTezosAddress.Should().Be("tz1Won");
        notification.Data.Should().BeEquivalentTo(new { domainName = "alice.tez" });
    }

    private void AssertAuctionLostNotification(Notification notification)
    {
        notification.Type.Should().Be(NotificationType.AuctionLost);
        notification.BlockLevel.Should().Be(2);
        notification.BlockTimestamp.Should().Be(Date2);
        notification.RecipientTezosAddress.Should().Be("tz1Lost");
        notification.Data.Should().BeEquivalentTo(new { domainName = "alice.tez" });
    }

    private void AssertOfferSoldNotification(Notification notification)
    {
        notification.Type.Should().Be(NotificationType.OfferSold);
        notification.BlockLevel.Should().Be(3);
        notification.BlockTimestamp.Should().Be(Date3);
        notification.RecipientTezosAddress.Should().Be("tz1Seller");
        notification.Data.Should().BeEquivalentTo(new { domainName = "alice.tez", price = "3" });
    }
    private void AssertBuyOfferPlacedNotification(Notification notification)
    {
        notification.Type.Should().Be(NotificationType.BuyOfferPlaced);
        notification.BlockLevel.Should().Be(4);
        notification.BlockTimestamp.Should().Be(Date4);
        notification.RecipientTezosAddress.Should().Be("tz1Owner");
        notification.Data.Should().BeEquivalentTo(new { domainName = "alice.tez", price = "4" });
    }
    private void AssertBuyOfferExecutedNotification(Notification notification)
    {
        notification.Type.Should().Be(NotificationType.BuyOfferExecuted);
        notification.BlockLevel.Should().Be(5);
        notification.BlockTimestamp.Should().Be(Date5);
        notification.RecipientTezosAddress.Should().Be("tz1Buyer");
        notification.Data.Should().BeEquivalentTo(new { domainName = "alice.tez" });
    }

    private async Task AssertState(DateTimeOffset lastTimestamp)
    {
        var state = await _stateService.Get(_ct);

        state.LastProcessedEventTimestamp.Should().Be(lastTimestamp);
    }

    private void AssertDefaultGQLRequests(DateTimeOffset? timestamp)
    {
        _tezosDomainsClientMock.ReceivedWithAnyArgs(5).EventsAsync(cancellationToken: default);

        AssertGQLRequest(0, null, timestamp);
        AssertGQLRequest(1, "1", timestamp);
        AssertGQLRequest(2, "2", timestamp);
        AssertGQLRequest(3, "3", timestamp);
        AssertGQLRequest(4, "4", timestamp);

    }

    private void AssertGQLRequest(int index, string expectedAfter, DateTimeOffset? expectedTimestamp)
    {
        var calls = _tezosDomainsClientMock.ReceivedCalls().Where(c => c.GetMethodInfo().Name == "EventsAsync").ToList();

        var call = calls[index];

        var args = call.GetArguments();

        var first = (StrawberryShake.Optional<int?>)args[0];
        var after = (StrawberryShake.Optional<string>)args[1];
        var where = (StrawberryShake.Optional<EventsFilter>)args[2];
        var order = (StrawberryShake.Optional<EventOrder>)args[3];
        var cancellationToken = (CancellationToken)args[4];

        first.Value.Should().Be(50);
        after.Value.Should().Be(expectedAfter);
        where.Value.Should()
            .BeEquivalentTo(
                new
                {
                    Type = new
                    {
                        Value = new
                        {
                            In = new
                            {
                                Value = new List<EventType> { EventType.AuctionBidEvent, EventType.AuctionEndEvent, EventType.OfferExecutedEvent, EventType.BuyOfferPlacedEvent, EventType.BuyOfferExecutedEvent },
                            }
                        }
                    },
                    Block = new
                    {
                        Value = new
                        {
                            Timestamp = new
                            {
                                Value = new
                                {
                                    GreaterThan = new
                                    {
                                        Value = expectedTimestamp
                                    }
                                }
                            }
                        }
                    }
                }
            );
        order.Value.Should()
            .BeEquivalentTo(
                new EventOrder
                {
                    Field = EventOrderField.Timestamp,
                    Direction = OrderDirection.Asc
                }
            );
        cancellationToken.Should().Be(_ct);
    }
}
