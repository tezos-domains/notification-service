﻿using FluentAssertions;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using MongoDB.Driver;
using NUnit.Framework;
using SendGrid.Helpers.Mail;
using TezosDomains.NotificationService.Services.Configuration;
using TezosDomains.NotificationService.Services.Model;
using TezosDomains.NotificationService.Services.SendGrid;

namespace TezosDomains.NotificationService.Services.IntegrationTests;

public class SubscriptionServiceTest : TestBase
{
    private SubscriptionService _service;
    private static readonly Guid Id1 = Guid.Parse("0f8fad5b-d9cb-469f-a165-70867728950e");
    private static readonly Guid Id2 = Guid.Parse("7c9e6679-7425-40de-944b-e07fc1f90ae7");
    private static readonly Guid Id3 = Guid.Parse("b7a4babe-56a5-11eb-ae93-0242ac130002");

    public class MockEmailConfiguration : IEmailConfiguration
    {
        public string BccLogAddress { get { return "bcc@my.domain"; } }
        public EmailAddress Sender => new();

        public IReadOnlyDictionary<string, string> Templates => new Dictionary<string, string>
        {
            { "Confirmation", "template" },
            { "AlreadySubscribed", "alreadySubscribedTemplate" }
        };

        public Uri AppUrl => new Uri("https://notifications.tezos.domains");
    }

    public class MockSubscriptionConfiguration : ISubscriptionConfiguration
    {
        public TimeSpan? ConfirmKeyExpiration => new TimeSpan(24, 0, 0);
    }

    [SetUp]
    public void SetUp()
    {
        _service = new SubscriptionService(
            DbContext,
            new MockSubscriptionConfiguration(),
            new SendGridMessageFactory(new MockEmailConfiguration()),
            new Logger<SubscriptionService>(new NullLoggerFactory())
        );
    }

    [Test]
    public async Task Subscribe_ShouldInsert()
    {
        // Act
        var message = await _service.Subscribe("test@test.com", "tz1aaa");

        // Assert
        var subs =
            await (await DbContext.Subscriptions.FindAsync(Builders<Subscription>.Filter.Empty)).ToListAsync();
        subs.SingleOrDefault().Should().NotBeNull();
        subs.Single().ConfirmedAt.Should().BeNull();
        subs.Single().UnsubscribedAt.Should().BeNull();
        subs.Single().ReplacedAt.Should().BeNull();
        subs.Single().Email.Should().Be("test@test.com");
        subs.Single().Address.Should().Be("tz1aaa");
        message.TemplateId.Should().Be("template");
    }

    [Test]
    public async Task Subscribe_ShouldNotInsertAndNotifyAboutExistingSubscription()
    {
        // Act
        await DbContext.Subscriptions.InsertOneAsync(
            new Subscription
            {
                Id = Guid.NewGuid(),
                UnsubscribeKey = Guid.NewGuid(),
                Email = "test@test.com",
                ConfirmedAt = DateTime.UtcNow.AddDays(-2),
                Address = "tz1already"
            }
        );

        var message = await _service.Subscribe("test@test.com", "tz1already");

        // Assert
        var subs =
            await (await DbContext.Subscriptions.FindAsync(Builders<Subscription>.Filter.Eq(s => s.Address, "tz1already"))).ToListAsync();
        subs.Should().HaveCount(1);
        message.TemplateId.Should().Be("alreadySubscribedTemplate");
    }

    [Test]
    public async Task Confirm_ShouldSetConfirmedState()
    {
        // Arrange
        await DbContext.Subscriptions.InsertOneAsync(
            new Subscription
            {
                Id = Id1,
                UnsubscribeKey = Id3,
                Email = "test@test.com",
                Address = "tz1blabla"
            }
        );

        // Act
        var result = await _service.Confirm(Id1);

        // Assert
        result.Email.Should().Be("test@test.com");
        result.Address.Should().Be("tz1blabla");
        var subs =
            await (await DbContext.Subscriptions.FindAsync(Builders<Subscription>.Filter.Empty)).ToListAsync();
        subs.SingleOrDefault().Should().NotBeNull();
        subs.Single().ConfirmedAt.Should().NotBeNull();
        subs.Single().UnsubscribedAt.Should().BeNull();
        subs.Single().ReplacedAt.Should().BeNull();
    }

    [Test]
    public async Task Confirm_ShouldReplacePrevious()
    {
        // Arrange
        await DbContext.Subscriptions.InsertOneAsync(
            new Subscription
            {
                Id = Id1,
                UnsubscribeKey = Id3,
                Email = "test@test.com",
                ConfirmedAt = DateTime.UtcNow,
                Address = "tz1blabla"
            }
        );
        await DbContext.Subscriptions.InsertOneAsync(
            new Subscription
            {
                Id = Id2,
                UnsubscribeKey = Id3,
                Email = "test@test.com",
                Address = "tz1blabla"
            }
        );

        // Act
        await _service.Confirm(Id2);

        // Assert
        var subs =
            await (await DbContext.Subscriptions.FindAsync(Builders<Subscription>.Filter.Empty)).ToListAsync();
        subs.Count().Should().Be(2);
        subs.Single(s => s.Id == Id1).ReplacedAt.Should().NotBeNull();
        subs.Single(s => s.Id == Id2).ConfirmedAt.Should().NotBeNull();
        subs.Single(s => s.Id == Id2).UnsubscribedAt.Should().BeNull();
        subs.Single(s => s.Id == Id2).ReplacedAt.Should().BeNull();
    }

    [Test]
    public void Confirm_ShouldThrowOnInvalidKey()
    {
        // Act
        var ex = Assert.ThrowsAsync<ServiceException>(async () => await _service.Confirm(Id2));

        // Assert
        ex.Reason.Should().Be(ServiceExceptionReason.KeyNotFound);
    }

    [Test]
    public async Task Confirm_ShouldThrowOnExpiredKey()
    {
        // Arrange
        await DbContext.Subscriptions.InsertOneAsync(
            new Subscription
            {
                Id = Id1,
                UnsubscribeKey = Id3,
                Email = "test@test.com",
                ConfirmedAt = DateTime.UtcNow.AddDays(-2),
                Address = "tz1blabla"
            }
        );

        // Act
        var ex = Assert.ThrowsAsync<ServiceException>(async () => await _service.Confirm(Id1));

        // Assert
        ex.Reason.Should().Be(ServiceExceptionReason.KeyExpired);
    }

    [Test]
    public async Task Unsubscribe_ShouldSetUnsubscribedState()
    {
        // Arrange
        DbContext.Subscriptions.InsertOne(
            new Subscription
            {
                Id = Id1,
                UnsubscribeKey = Id3,
                Email = "test@test.com",
                ConfirmedAt = DateTime.UtcNow,
                Address = "tz1blabla"
            }
        );

        // Act
        var result = await _service.Unsubscribe(Id3);

        // Assert
        var subs =
            await (await DbContext.Subscriptions.FindAsync(Builders<Subscription>.Filter.Empty)).ToListAsync();
        subs.SingleOrDefault().Should().NotBeNull();
        subs.Single().UnsubscribedAt.Should().NotBeNull();

        result.Should().NotBeNull();
        result.Address.Should().Be("tz1blabla");
        result.Email.Should().Be("test@test.com");
    }

    [Test]
    public void Unsubscribe_ShouldThrowOnInvalidKey()
    {
        // Act
        var ex = Assert.ThrowsAsync<ServiceException>(async () => await _service.Unsubscribe(Id2));

        // Assert
        ex.Reason.Should().Be(ServiceExceptionReason.KeyNotFound);
    }
}
