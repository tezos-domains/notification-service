﻿using Microsoft.Extensions.Configuration;
using MongoDB.Driver;
using NUnit.Framework;
using TezosDomains.NotificationService.Services.Configuration;

namespace TezosDomains.NotificationService.Services.IntegrationTests;

public abstract class TestBase
{
    public class TestMongoDbConfiguration : IMongoDbConfiguration
    {
        public string ConnectionString { get; set; }

        private static readonly string DatabaseName = $"t_{Guid.NewGuid()}";

        string IMongoDbConfiguration.ConnectionString =>
            new MongoUrlBuilder(ConnectionString)
            {
                DatabaseName = DatabaseName
            }.ToString();
    }

    protected MongoDbContext DbContext;

    protected IConfigurationRoot Configuration;

    protected IMongoDbConfiguration MongoDbConfiguration =>
        Configuration.GetSection("MongoDb").Get<TestMongoDbConfiguration>();

    [SetUp]
    public void BaseSetUp()
    {
        Configuration = new ConfigurationBuilder()
            .AddUserSecrets<TestBase>()
            .AddEnvironmentVariables()
            .Build();
        DbContext = new MongoDbContext(MongoDbConfiguration);
    }

    [TearDown]
    public void BaseTearDown()
    {
        DbContext.Client.DropDatabase(MongoUrl.Create(MongoDbConfiguration.ConnectionString).DatabaseName);
    }
}
