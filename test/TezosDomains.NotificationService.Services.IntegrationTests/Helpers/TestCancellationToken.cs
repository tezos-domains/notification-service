﻿namespace TezosDomains.NotificationService.Services.IntegrationTests.Helpers;

internal static class TestCancellationToken
{
    /// <summary>
    ///     Create a fake using CTS in order to be unique each time.
    /// </summary>
    public static CancellationToken Get()
    {
        return new CancellationTokenSource().Token;
    }
}
